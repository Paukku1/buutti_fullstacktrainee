import request from 'supertest'
import userRouter from '../src/userRouter'
import  express  from 'express'


const app = express()
app.use(express.json());
app.use('/api/v1/users', userRouter)

describe('Server', () => {

    it('Returns 404 on invalid address', async () => {
        const response = await request(app)
            .get('/invalidaddress')
        expect(response.statusCode).toBe(404)
    })

})

describe('Register', () => {

    it('Returns 401 if missing password', async () => {
        
        const payload = {username: 'Katie' }
        const response = await request(app)
            .post('/api/v1/users/register')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 401 if missing username', async () => {
        
        const payload = {password: '12345' }
        const response = await request(app)
            .post('/api/v1/users/register')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 401 if usrname already exist', async () => {
        
        const payload = {username: 'Admin', password: '12345' }
        const response = await request(app)
            .post('/api/v1/users/register')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 200 when send data which is correct', async () => {
        
        const payload = {username: 'Katie', password: '2342388' }
        const response = await request(app)
            .post('/api/v1/users/register')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(200)
    })

})

describe('Login', () => {

    it('Returns 401 if missing password', async () => {
        
        const payload = {username: 'Katie' }
        const response = await request(app)
            .post('/api/v1/users/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 401 if missing username', async () => {
        
        const payload = {password: '12345' }
        const response = await request(app)
            .post('/api/v1/users/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 404 when username can not find', async () => {
        
        const payload = {username: 'Kerttu', password: '2342388' }
        const response = await request(app)
            .post('/api/v1/users/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(404)
    })

    it('Returns 401 if password is incorrect', async () => {
        
        const payload = {username: 'Katie', password: '2' }
        const response = await request(app)
            .post('/api/v1/users/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 200 when send data which is correct', async () => {
        
        const payload = {username: 'Katie', password: '2342388' }
        const response = await request(app)
            .post('/api/v1/users/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(200)
    })

    it('Returns 200 when send data which is correct', async () => {
        
        const payload = {username: 'Admin', password: 'SecretPassword' }
        const response = await request(app)
            .post('/api/v1/users/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(200)
    })
})
