import request from 'supertest'
import bookRouter from '../src/booksRouter'
import userRouter from '../src/userRouter'
import  express  from 'express'

const app = express()
app.use(express.json());
app.use('/api/v1/users', userRouter)
app.use('/api/v1/books', bookRouter)


async function getAuthTokenForUser(username: string, password: string): Promise<string> {
  await request(app)
    .post('/api/v1/users/register')
    .send({ username, password })

  const loginResponse = await request(app)
    .post('/api/v1/users/login')
    .send({ username, password })

  expect(loginResponse.status).toBe(200)
  return loginResponse.body.token
}

async function getAuthTokenForAdmin(): Promise<string> {
  const loginResponse = await request(app)
    .post('/api/v1/users/login')
    .send({username: "Admin", password: "SecretPassword" })

  expect(loginResponse.status).toBe(200)
  return loginResponse.body.token
}

describe('Invalid token', () => {
    let token: string;
  
    beforeAll(async () => {
      token = await getAuthTokenForUser('testuser', 'testpassword')
    })
  
    it('should return 401 Forbidden if token is invalid', async () => {
      const response = await request(app)
        .get('/api/v1/books/')
        .set('Authorization', 'Bearer invalid_token')
      expect(response.status).toBe(401)
    })
  })

describe('Book Routes for regular user what user can use.', () => {
    let token: string;
  
    beforeAll(async () => {
      token = await getAuthTokenForUser('testuser', 'testpassword')
    })
  
    it('should return 200 OK if token is valid', async () => {
      const bookResponse = await request(app)
        .get('/api/v1/books/')
        .set('Authorization', `Bearer ${token}`)
  
      expect(bookResponse.status).toBe(200)
    })
  
    it('should return 200 OK when retrieving a specific book', async () => {
      const bookResponse = await request(app)
        .get('/api/v1/books/1')
        .set('Authorization', `Bearer ${token}`)
  
      expect(bookResponse.status).toBe(200)
    })
  })

describe('Book not found. Post, put and delete for regular user', () => {
    let token: string;
  
    beforeAll(async () => {
      token = await getAuthTokenForUser('testuser', 'testpassword')
    })
  
    it('should return 404 when book not found', async () => {
        const bookResponse = await request(app)
          .get('/api/v1/books/2')
          .set('Authorization', `Bearer ${token}`)
    
        expect(bookResponse.status).toBe(404)
      })
  
    it('should return 403 Forbidden when creating a book as a non-admin user', async () => {
      const bookResponse = await request(app)
        .post('/api/v1/books/')
        .set('Authorization', `Bearer ${token}`)
  
      expect(bookResponse.status).toBe(403)
})
  
    it('should return 403 Forbidden when updating a book as a non-admin user', async () => {
      const bookResponse = await request(app)
        .put('/api/v1/books/1')
        .set('Authorization', `Bearer ${token}`)
        .send({ name: 'Welcome' })
  
      expect(bookResponse.status).toBe(403)
    })
  
    it('should return 403 Forbidden when deleting a book as a non-admin user', async () => {
      const bookResponse = await request(app)
        .delete('/api/v1/books/1')
        .set('Authorization', `Bearer ${token}`)
  
      expect(bookResponse.status).toBe(403)
    })
  })


describe('Valid and invalid token for Admin', () => {
    let token: string;
  
    beforeAll(async () => {
      token = await getAuthTokenForAdmin()
    })
  
    it('should return 401 Forbidden if token is invalid', async () => {
      const response = await request(app)
        .get('/api/v1/books/')
        .set('Authorization', 'Bearer invalid_token')
      expect(response.status).toBe(401)
    })
  
    it('should return 200 OK if token is valid', async () => {
      const bookResponse = await request(app)
        .get('/api/v1/books/')
        .set('Authorization', `Bearer ${token}`)
  
      expect(bookResponse.status).toBe(200)
    })
})

describe('Valid get, post, put and delete for Admin', () => {
    let token: string;
  
    beforeAll(async () => {
      token = await getAuthTokenForAdmin()
    })
  
    it('should return 200 OK when retrieving a specific book', async () => {
      const bookResponse = await request(app)
        .get('/api/v1/books/1')
        .set('Authorization', `Bearer ${token}`)
  
      expect(bookResponse.status).toBe(200)
    })
  
    it('should return 200 Forbidden when creating a book as an admin user', async () => {
        const book = {id: 3, name: "Hi there", author: "Venn", read: "Billy"}
        const bookResponse = await request(app)
            .post('/api/v1/books/')
            .set('Authorization', `Bearer ${token}`)
            .send(book)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')

      expect(bookResponse.status).toBe(200)
    })

    it('should return 200 Forbidden when updating a book as an admin user', async () => {
      const bookResponse = await request(app)
        .put('/api/v1/books/1')
        .set('Authorization', `Bearer ${token}`)
        .send({ name: 'Welcome' })
  
      expect(bookResponse.status).toBe(200)
    })
  
    it('should return 200 Forbidden when deleting a book as an admin user', async () => {
      const bookResponse = await request(app)
        .delete('/api/v1/books/1')
        .set('Authorization', `Bearer ${token}`)
        .send()
  
      expect(bookResponse.status).toBe(200)
    })
})

describe('Invalid get, post, put and delete for Admin', () => {
    let token: string;
  
    beforeAll(async () => {
      token = await getAuthTokenForAdmin()
    })
  
    it('should return 404 when book not found', async () => {
        const bookResponse = await request(app)
          .get('/api/v1/books/2')
          .set('Authorization', `Bearer ${token}`)
    
        expect(bookResponse.status).toBe(404)
      })

    it('should return 400 Forbidden when creating a book as an admin user and missing something', async () => {
        const book = {id: 3, name: "Hi there", read: "Billy"}
        const bookResponse = await request(app)
            .post('/api/v1/books/')
            .set('Authorization', `Bearer ${token}`)
            .send(book)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        
      expect(bookResponse.status).toBe(400)
    })
  
    it('should return 400 Forbidden when updating a book as an admin user and missing something', async () => {
        const bookResponse = await request(app)
          .put('/api/v1/books/1')
          .set('Authorization', `Bearer ${token}`)
          .send()
    
        expect(bookResponse.status).toBe(400)
      })
  
    it('should return 404 Forbidden when deleting a book as an admin user and id not found', async () => {
        const bookResponse = await request(app)
          .delete('/api/v1/books/5')
          .set('Authorization', `Bearer ${token}`)
          .send({hello: 'name'})
    
        expect(bookResponse.status).toBe(404)
      })
})