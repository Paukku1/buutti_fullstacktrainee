import express, { Request, Response, NextFunction, Router } from 'express'
import { middleware, unknownEndpoint } from './middlewares'
import argon2 from 'argon2'

const router = express.Router()
router.use(middleware)

interface User {
    username: string,
    password: string
}

let users: User[] = []

router.post('/', async (req: Request, res: Response) => {
    const {username, password} = req.body
    
    if(username === undefined || password === undefined) {
        return res.status(401).send({error: 'Need username and password'})
    } 
    
    const newPassword = await argon2.hash(password).then(result => result)  
    const newUser: User = {username : username, password :newPassword}
    users.push(newUser)
    console.log(users)     
    
    res.status(201).send('Success')
    
})

router.post('/login', async (req: Request, res: Response) => {
    const {username, password} = req.body
    if(username === undefined || password === undefined) return res.status(401).send({error: 'Need username and password'}) 

    const user = users.find(user => user.username === username)
    if(user === undefined) return res.status(404).send({error: "Username is incorrect"})

    const verifyPassword = await argon2.verify(user.password, password)
    if(!verifyPassword) return res.status(401).send({error: 'Wrong password'})

    res.status(204).send()
    
})

router.use(unknownEndpoint)
  
export default router