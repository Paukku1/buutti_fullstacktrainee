// npm install --save-dev @types/jsonwebtoken
import 'dotenv/config'
import jwt from 'jsonwebtoken'

const payload = { username: 'Chocolatecake' }
const secret = process.env.SECRET ?? ''
const options = { expiresIn: '15min'}

const token = jwt.sign(payload, secret, options)
console.log(token)

const myToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkNob2NvbGF0ZSIsImlhdCI6MTY4NTM2MzczNiwiZXhwIjoxNjg1MzY0NjM2fQ.ZjgZ2o8LEAuAJEBYeLX-KSfcwUYJMSMEuIYEr7B64Nk'

const result = jwt.verify(myToken, secret)

console.log(result)