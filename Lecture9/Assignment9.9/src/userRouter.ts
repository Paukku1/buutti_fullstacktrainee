import express, { Request, Response} from 'express'
import { middleware, unknownEndpoint } from './middlewares'
import argon2 from 'argon2'
import 'dotenv/config'
import jwt from 'jsonwebtoken'

const router = express.Router()
router.use(middleware)

interface User {
    username: string,
    password: string
}

let users: User[] = []

router.post('/register', async (req: Request, res: Response) => {
    const {username, password} = req.body 
    
    if(username === undefined || password === undefined) {
        return res.status(401).send({error: 'Need username and password'})
    } 
    
    const newPassword = await argon2.hash(password).then(result => result)  
    const newUser: User = {username : username, password :newPassword}
    users.push(newUser)
    console.log(users)     
    
    res.status(200).send('Success')
    
})

router.post('/login', async (req: Request, res: Response) => {
    const {username, password} = req.body
    if(username === undefined || password === undefined) return res.status(401).send({error: 'Need username and password'}) 

    const user = users.find(user => user.username === username)
    if(user === undefined) return res.status(404).send({error: "Username is incorrect"})

    const verifyPassword = await argon2.verify(user.password, password)
    if(!verifyPassword) return res.status(401).send({error: 'Wrong password'})

    const payload = { username: username }
    const secret = process.env.SECRET ?? ''
    const options = { expiresIn: '1h'}

    const token = jwt.sign(payload, secret, options)

    res.status(200).send(token)
    
})

router.post('/admin', async (req: Request, res: Response) => {
    const {username, password} = req.body
    if(username === undefined || password === undefined) return res.status(401).send({error: 'Need username and password'}) 

    const {ADMIN, PASSWORD} = process.env
    const adminHash = ADMIN ?? ''
    const hashPassword = PASSWORD ?? ''

    if(username !== adminHash) return res.status(401).send({error: 'Username is incorrect'})
    
    const verifyPassw = await argon2.verify(hashPassword, password)

    if(!verifyPassw) return res.status(401).send({error: 'Password is incorrect'})

    const payload = { username: username }
    const secret = process.env.ADMINSECRET ?? ''
    const options = { expiresIn: '1h'}

    const token = jwt.sign(payload, secret, options)

    res.status(200).send(token)
})

router.use(unknownEndpoint)
  
export default router