import express, { Request, Response, NextFunction, Router } from 'express'
import { middleware, authenticate, adminAuthenticate, unknownEndpoint } from './middlewares'

const router = express.Router()
router.use(middleware)

interface Student {
    id: number,
    name: string,
    email: string
  }
  let students: Student[] = []
  
  router.get('/', authenticate, (req: Request, res: Response) => {
    
    if(students.length === 0) {
        res.status(404).send({error: 'No students found'})
        return
    } else {
        const newArr = students.map(student => student.id)
        res.send(newArr)
        return
    }
  })
  
  router.get('/:id', authenticate, (req: Request,  res: Response, next) => {
    students.find(student => {
      if(student.id.toString() === req.params.id) {
        res.send(student)
        return
      }
    })
    next()
  })
  
  router.post('/', adminAuthenticate, (req: Request, res: Response) => {
    if(!req.body.id || !req.body.name || !req.body.email) {
      res.status(400).send({error: "Some of parameters is missing"})
    } else {
      const newStudent: Student = {
        id : req.body.id,
        name: req.body.name,
        email: req.body.email
      }
      students.push(newStudent)
      res.status(200).send(students) 
    }
  })
  
  router.put('/:id', adminAuthenticate, (req: Request, res: Response, next) => {
    const id = req.params.id
    const { name, email } = req.body
    const student = students.find(student => student.id.toString() === id)
  
    if (student) {
      if(!name && !email){
        res.status(400).send({error: 'Email and name missing. Add one of them'})
      } else {
        if (name) {
          student.name = name
        }
        if (email) {
          student.email = email
        }
        res.status(204).send()
      }
    } 
    next()
  })
  
  router.delete('/:id', adminAuthenticate, (req: Request, res: Response, next) => {
    const id = req.params.id
    const student = students.find(student => student.id.toString() === id)
    
    if(student){ 
      students = students.filter(student => student.id.toString() !== id)
      res.status(204).send()
    }
  
    next()
  })
  
  router.use(unknownEndpoint)
  
  export default router