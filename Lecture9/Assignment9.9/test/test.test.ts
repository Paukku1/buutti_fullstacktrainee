import request from 'supertest'
import router from '../src/userRouter'
import  express  from 'express'


const app = express()
app.use(express.json());
app.use(router)

describe('Server', () => {

    it('Returns 404 on invalid address', async () => {
        const response = await request(app)
            .get('/invalidaddress')
        expect(response.statusCode).toBe(404)
    })

})

describe('Register', () => {

    it('Returns 401 if missing password', async () => {
        
        const payload = {username: 'john' }
        const response = await request(app)
            .post('/register')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 401 if missing username', async () => {
        
        const payload = {password: 'john' }
        const response = await request(app)
            .post('/register')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 200 when send data which is correct', async () => {
        
        const payload = {username: 'john', password: '2342388' }
        const response = await request(app)
            .post('/register')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(200)
    })


})


describe('login', () => {

    it('Returns 401 if missing password', async () => {
        
        const payload = {username: 'john' }
        const response = await request(app)
            .post('/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 401 if missing username', async () => {
        
        const payload = {password: 'john' }
        const response = await request(app)
            .post('/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 404 when username can not find', async () => {
        
        const payload = {username: 'Kerttu', password: '2342388' }
        const response = await request(app)
            .post('/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(404)
    })

    it('Returns 401 if password is incorrect', async () => {
        
        const payload = {username: 'john', password: '2' }
        const response = await request(app)
            .post('/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(401)
    })

    it('Returns 200 when send data which is correct', async () => {
        
        const payload = {username: 'john', password: '2342388' }
        const response = await request(app)
            .post('/login')
            .send(payload)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
        expect(response.statusCode).toBe(200)
    })
})