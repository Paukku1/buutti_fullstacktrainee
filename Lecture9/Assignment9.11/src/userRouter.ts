import express, { Request, Response } from 'express'
import { loggerMiddleware } from './middlewares'
import argon2 from 'argon2'
import jwt from 'jsonwebtoken'
import 'dotenv/config'

const router = express.Router()
router.use(express.json())
router.use(loggerMiddleware)

interface User {
    username: string,
    password: string
}

const adminHashPassword = process.env.ADMINPASSWORD ?? ''
let users: User[] = [{username: 'Admin', password: ''}]

router.post('/register',  async (req: Request, res: Response) => {
    const {username, password} = req.body 
    
    if(username === undefined || password === undefined) {
        return res.status(401).send({error: 'Need username and password'})
    } 
    
    const checkUser = users.find(user => user.username === username)
    if(checkUser) return res.status(401).send({error: 'username already exist'}) 

    const newPassword = await argon2.hash(password).then(result => result)  
    const newUser: User = {username : username, password :newPassword}
    users.push(newUser)
    
    const payload = { username: username }
    const secret = process.env.SECRET ?? ''
    const options = { expiresIn: '1h'}

    const token = jwt.sign(payload, secret, options)
    
    res.status(200).send(token)
})

router.post('/login', async (req: Request, res: Response) => {
    const {username, password} = req.body 

    if(username === undefined || password === undefined) {
        return res.status(401).send({error: 'Need username and password'})
    } 

    const checkUser = users.find(user => user.username === username)
    if(checkUser === undefined) return res.status(404).send({error: 'Username is incorrect'})

    let verifyPassword
    if(checkUser.username === 'Admin') {
        verifyPassword = await argon2.verify(adminHashPassword, password)
        
    } else {
        verifyPassword = await argon2.verify(checkUser.password, password)
    }
    
    if(!verifyPassword) return res.status(401).send({error: 'Wrong password'})

    let payload: { username: any, isAdmin?: boolean } = { username: username }
    const secret = process.env.SECRET ?? ''
    let options = { expiresIn: '1h'}

    if(checkUser.username === 'Admin') {
        payload = { username: username, isAdmin: true}
    }

    const token = jwt.sign(payload, secret, options)
    
    res.status(200).send(token)

})

export default router