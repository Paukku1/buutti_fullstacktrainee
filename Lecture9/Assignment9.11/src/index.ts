import express from 'express'
import {loggerMiddleware } from './middlewares'
import books from './booksRouter'
import userRouter from './userRouter'

const server = express()
server.use(express.json())
server.use(loggerMiddleware)
server.use('/api/v1/books', books)
server.use('/api/v1/users', userRouter)


server.listen(3000)
