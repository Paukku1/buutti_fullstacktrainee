import { Request, Response, NextFunction } from 'express'
import jwt, { JwtPayload } from 'jsonwebtoken'

interface ObjectWithRequiredFields {
  id: number,
  name: string,
  author: string,
  read: string
}
interface CustomRequest extends Request {
  user?: JwtPayload
}

export const loggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const timestamp = new Date().toISOString()
  const method = req.method
  const url = req.originalUrl

  console.log(`[${timestamp}] ${method} ${url}`)

  next()
}

export const authenticate = (req:CustomRequest, res: Response, next: NextFunction) => {
  const auth = req.get('Authorization')
  
  if (!auth?.startsWith('Bearer ')) {
      return res.status(401).send('Invalid token')
  }

  const token = auth.substring(7)
  const secret = process.env.SECRET ?? ''
  
  try {
      const decodedToken = jwt.verify(token, secret) as JwtPayload
      req.user = decodedToken
      next()
  } catch (error) {
      return res.status(401).send('Invalid token')
  }
}

export const authorizeAdmin = (req: CustomRequest, res: Response, next: NextFunction) => {
  const auth = req.get('Authorization')
  
  if (!auth?.startsWith('Bearer ')) {
    return res.status(401).send('Invalid token')
  }

  const token = auth.substring(7)
  const secret = process.env.SECRET ?? ''

  try {
      const decodedToken = jwt.verify(token, secret) as JwtPayload
      req.user = decodedToken
  } catch (error) {
      return res.status(401).send('Invalid token')
  }

  if (req.user.isAdmin === undefined) {
    return res.status(403).json({ error: 'Admin access required' });
  }
  next()
}

export const validatorPost = ( req: Request, res: Response, next: NextFunction) => {
    const body = req.body

    const requiredKeys: (keyof ObjectWithRequiredFields)[] = ['id', 'name', 'author', 'read'];

    for (const key of requiredKeys) {
      if (!(key in body)) {
        res.status(400).send({ error: `Missing required field: ${key}` })
        return
      }
    }
      next()
  }

export const validatorPut = ( req: Request, res: Response, next: NextFunction) => {
    const {name, author, read} = req.body

    if (!name && !author && !read) {
      res.status(400).send({ error: 'Missing required field' });
      return
    }
  
    next()
  }

export const error404 = (req: Request, res: Response, next: NextFunction) => {
  res.status(404).send({error: 'no book for that ID'})

}
