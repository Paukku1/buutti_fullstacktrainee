import express, { Request, Response, NextFunction } from 'express'
import { validatorPost, error404, validatorPut, authenticate, authorizeAdmin, loggerMiddleware } from './middlewares'
import helmet from "helmet"

const router = express.Router()
router.use(express.json())
router.use(loggerMiddleware)
router.use(helmet())

interface Book {
  id: number,
  name: string,
  author: string,
  read: string
}

let books: Book[] = []

router.get('/', authenticate, (req: Request, res: Response) => {
  res.status(200).send(books)
})

router.get('/:id', authenticate, (req: Request, res: Response, next: NextFunction) => {
  const id = parseInt(req.params.id)
  const book = books.find(book => book.id === id)
  if(book) {
    res.status(200).send(book)
    return
  }
  next()
})

router.post('/', authorizeAdmin, validatorPost, (req: Request, res: Response) => {
  validatorPost
  books.push(req.body)
  res.status(200).send()
})

router.put('/:id', authorizeAdmin, validatorPut, (req: Request, res: Response, next: NextFunction) => {
  const id = parseInt(req.params.id)
  const {name, author, read} = req.body
  const book = books.find(book => book.id === id)
  
  if(book === undefined) {
    next()
    return
  }
  else{
    router.use(validatorPut)
  }
  if(name) {
    book.name = name
  }
  if(author) {
    book.author = author
  }
  if(read) {
    book.read = read
  }
  res.status(200).send(book)
  
})

router.delete('/:id', authorizeAdmin, (req: Request, res: Response, next: NextFunction) =>{
  const id = parseInt(req.params.id)
  const book = books.find(book => book.id === id)
  if(!book) {
    next()
    return
  } else {
    books = books.filter(book => book.id !== id)
  }
  res.status(200).send()
})

router.use(error404)

export default router
