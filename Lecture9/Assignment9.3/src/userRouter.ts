import express, { Request, Response, NextFunction, Router } from 'express'
import { middleware, unknownEndpoint } from './middlewares'
import argon2 from 'argon2'

const router = express.Router()
//router.use(middleware)

interface User {
    username: string,
    password: string
}

let users: User[] = []

router.post('/', async (req: Request, res: Response) => {
    console.log('This is root')
    const {username, password} = req.body
    if(!username || !password) {
        res.status(400).send({error: 'Need username and password'})
        return 
    } 

    const newPassword = await argon2.hash(password).then(result => result)  
    const newUser: User = {username : username, password :newPassword}
    users.push(newUser)
    console.log(users)     
    
    res.status(201).send('Success')
    
})

router.use(unknownEndpoint)
  
export default router