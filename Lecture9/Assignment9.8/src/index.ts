import 'dotenv/config'
import express, { Request, Response, NextFunction } from 'express'
import studentRouter from './studentRouter'
import userRouter from './userRouter'
import { unknownEndpoint } from './middlewares'

const server = express()
server.use(express.static('public'))
server.use(express.json())
server.use('/student', studentRouter)
server.use('/user', userRouter)
server.use(unknownEndpoint)
const PORT = process.env.PORT

server.listen(PORT, () => {
  console.log('Listening to port ', PORT)
})



