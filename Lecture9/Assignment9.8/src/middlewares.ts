import 'dotenv/config'
import jwt, { JwtPayload } from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express'

interface CustomRequest extends Request {
  user?: JwtPayload
}


export const middleware = (req: Request, res: Response, next: NextFunction) => { 
  if(!req.body) {
    console.log("No reg.body")
  }
  else {
    console.log(req.body)
  }
    const time = new Date()
    console.log(time.getHours().toString().padStart(2, '0'), time.getMinutes().toString().padStart(2, '0'))
    console.log(req.method)
    console.log(req.path)
    console.log(time)
    next()
  }

  export const authenticate = (req:CustomRequest, res: Response, next: NextFunction) => {
    const auth = req.get('Authorization')
    if (!auth?.startsWith('Bearer ')) {
        return res.status(401).send('Invalid token')
    }
    const token = auth.substring(7)
    const secret = process.env.SECRET ?? ''
    try {
        const decodedToken = jwt.verify(token, secret) as JwtPayload
        req.user = decodedToken
        next()
    } catch (error) {
        return res.status(401).send('Invalid token')
    }
}

export const adminAuthenticate = (req:CustomRequest, res: Response, next: NextFunction) => {
  const auth = req.get('Authorization')
  if (!auth?.startsWith('Bearer ')) {
      return res.status(401).send('Invalid token')
  }
  const token = auth.substring(7)
  const secret = process.env.ADMINSECRET ?? ''
  try {
      const decodedToken = jwt.verify(token, secret) as JwtPayload
      req.user = decodedToken
      next()
  } catch (error) {
      return res.status(401).send('Invalid token')
  }
}



  export const unknownEndpoint = ( req: Request, res: Response) => {
    res.status(404).send({ error: "No one here"})
}