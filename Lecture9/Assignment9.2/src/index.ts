import express, { Request, Response, NextFunction } from 'express'
import studentRouter from './studentRouter'
import { unknownEndpoint } from './middlewares'

const server = express()
server.use(express.static('public'))
server.use(express.json())
server.use('/student', studentRouter)
server.use(unknownEndpoint)

server.listen(3000, () => {
  console.log('Listening to port 3000')
})



