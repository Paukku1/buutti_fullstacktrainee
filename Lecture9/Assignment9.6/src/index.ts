// npm install --save-dev @types/jsonwebtoken

import 'dotenv/config'
import jwt from 'jsonwebtoken'

const payload = { username: 'Chocolatecake' }
const secret = process.env.SECRET ?? ''
const options = { expiresIn: '1h'}

const token = jwt.sign(payload, secret, options)
console.log(token)
