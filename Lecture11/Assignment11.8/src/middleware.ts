import { Request, Response, NextFunction } from 'express'
import {Events} from './index'

export const checkIsValidtoPost = (req: Request, res: Response, next: NextFunction) => {
    const {title, description, time} = req.body
    let date = req.body.date

    if(title === undefined || description === undefined || date === undefined) return res.status(404).send({error: 'title, description or date is missing'})
    
    const datePattern1 = /^(\d{2})[.\\/](\d{2})[.\\/](\d{4})$/
    const timePattern1 = /^(\d{2})[:\\/](\d{2})$/

    const match1 = date.match(datePattern1)

    if (!match1) return res.status(400).send({error: 'Date is incorrect. Correct is: dd/mm/yyyy or dd.mm.yyyy'})

    if(time) {
        const matchtime = time.match(timePattern1)
        if (!matchtime) return res.status(400).send({error: 'Time is incorrect. Correct is: hh:mm'})
    }
    const day = parseInt(match1[1], 10)
    const month = parseInt(match1[2], 10)
    const year = parseInt(match1[3], 10)
    date = new Date(year, month - 1, day)

    res.locals.title = title
    res.locals.description = description
    res.locals.date = date
    res.locals.time = time

    next()
}

export function timeParser (eventsCalender: Events[]) {
    const calender = eventsCalender.map(event => ({
        ...event,
        date: event.date.toLocaleDateString('fi-FI')
    }))
    return calender
}