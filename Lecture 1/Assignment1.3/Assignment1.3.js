const firstNumber = 6
const secondNumber = 3

console.log("sum", firstNumber + secondNumber)
console.log("difference", firstNumber - secondNumber)
console.log("product", firstNumber * secondNumber)
console.log("fraction", firstNumber / secondNumber)

// Extra 1 
console.log("exponentiation", firstNumber ** secondNumber)
console.log("modulo", firstNumber % secondNumber)

// Extra 2
console.log("average", (firstNumber + secondNumber) / 2)