const distance = 100
const speed = 60
let traveltime = distance / speed * 60
let hours = 0
let min = 0

while(traveltime > 60 ) {
    hours++
    traveltime = traveltime - 60
}
min = traveltime
// Luova ratkaisu :) 
// Modulus olisi kuitenkin hoitanut homman tehokkaammin

console.log(`Travel time is ${hours} hours and ${min} minutes`)
