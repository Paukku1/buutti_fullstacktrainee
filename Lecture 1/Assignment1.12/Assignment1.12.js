let numberOfPeople = process.argv[2]
const groupSize = process.argv[3]
let groups = 0

while(numberOfPeople >= 1) {
    numberOfPeople = numberOfPeople - groupSize
    groups++
}
// Täälläkin modulus hoitaa homman ilman looppia

console.log("Number of groups", groups)