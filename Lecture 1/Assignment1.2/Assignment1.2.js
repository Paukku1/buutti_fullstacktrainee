const stringi = "Hello world"
const numero = 13
let bool = true // This variable can change

console.log( typeof(stringi), stringi)
console.log( typeof(numero), numero)
console.log( typeof(bool), bool)

bool = false

console.log( typeof(stringi), stringi)
console.log( typeof(numero), numero)
console.log( typeof(bool), bool)