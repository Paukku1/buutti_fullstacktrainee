const lastName = "LastName"
let age = 60
const isDoctor = false
const sender = "Me mario"
let end = "th"

isDoctor
    ? console.log(`Dear Dr. ${lastName}` )
    : console.log(`Dear Mx. ${lastName}` )

age++
if(age % 10 === 1) {
    end = "st"
}
else if(age % 10 === 2) {
    end = "nd"
}
else if(age % 10 === 3) {
    end = "rd"
}

console.log(`Congratulations on your ${age}${end} birthday! Many happy returns!
Sincerely
${sender}`)