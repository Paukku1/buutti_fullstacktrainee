// 1.5 a
const playerCount = 4

if(playerCount === 4) {
    console.log("You can play!")
}
else {
    console.log("You can't play")
}

// 1.5 b
const isStressed = true
const hasIcecream = true

if(!isStressed || hasIcecream) {
    console.log("Mark is happy")
}
else {
    console.log("Mark is not happy")
}

// 1.5 c
const sunShine = true
const raining = false
const tempature = 22

if(sunShine && !raining && tempature >= 20) {
    console.log("It is beach day!")
}
else {
    console.log("Stay inside and code")
}

// 1.5 d

const seeSuzy = false
const seeDan = true
const weekDay = "Tuesday"
const clockTime = "Night"

if(weekDay === "Tuesday" && clockTime === "Night" && seeDan && !seeSuzy || seeSuzy && !seeDan) {
    console.log("Arin is happy!")
} 
else {
    console.log("Arin is sad")
}

// Viimeisessä saattaa mennä logiikka sekaisin, koska ei ole sulkeita. 
// Voit käyttää || ja && operaattoreita huoletta ilman sulkeita, jos ehtolauseessa on vain yhdenlaisia operaattoreita. Jos lauseessa on sekä && että || jää toivottu järjestys epäselväksi ja koodiin tulee helposti bugeja, koska kone ymmärtää asiat eri järjestyksessä kuin ihminen.