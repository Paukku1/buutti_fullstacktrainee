const price = 140
const discount = 35

console.log(`Original price: ${price}e
Discount percentage: ${discount}%
Discounted price: ${price - (price * (discount/100))}e`)