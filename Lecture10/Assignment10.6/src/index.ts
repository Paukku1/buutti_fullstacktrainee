import express, { Request, Response, NextFunction } from 'express'

const server = express()
server.use(express.json())

server.get('/', (reg: Request, res: Response) => {
    res.status(200).send('Hello from Docker assignment10.6!')
})

server.listen(3000)