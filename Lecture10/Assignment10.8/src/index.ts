let a = 0
let b = 1

const fibonacciSequence = () => {
    const current = a
    a = b
    b = current + b
    return current
}

console.log("Whale Read Fibonacci")

setInterval(() => {
    const number = fibonacciSequence()
    console.log(number)
}, 1000)
