import express from "express"
import nodeRouter from "./nodeRouter"

const server = express()
server.use(express.json())
server.use("/notes", nodeRouter)

server.listen(3000, () => console.log("Listening to port", 3000))