import express from "express"

const router = express.Router()
router.use(express.static("public"))

interface Note {
    id: number
    note: string
}
let notes: Note[] = []

router.get("/", (req, res) => {
    res.status(200).send(notes)
})

router.post("/", (req, res) => {
    const note = req.body.note
    if(note === undefined || note === "") return res.status(400).send("note can not be empty")

    const id = notes.length + 1
    notes.push({id, note})

    res.status(200).send(notes)
})

router.delete("/:id", (req, res) => {
    const id = parseInt(req.params.id)
    if(notes.length < 1) return res.status(400).send("Nothing in the array")
    
    const findId = notes.find(note => note.id === id)
    if(findId === undefined)return res.status(404).send("id did not found")

    notes = notes.filter(note => note.id !== id)

    res.status(200).send(notes)

})

export default router