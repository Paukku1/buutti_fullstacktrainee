import { Request, Response, NextFunction } from "express"
import argon2  from "argon2"
import "dotenv/config"

const verifyUsername = process.env.SECRETUSERNAME ?? ""
const verifyhashPassword = process.env.PASSWORD ?? ""

export const authenticate = async (req:Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers.authorization
    const {username, password} = req.body
    let isValid = false

    if(username === undefined || password === undefined) {
        if (!authHeader || !authHeader.startsWith("Basic ")) {
            res.setHeader("WWW-Authenticate", "Basic realm=\"Authentication required\"")
            res.status(401).send("Authentication required")
            return
        }

        const encodedCredentials = authHeader.replace("Basic ", "")
        const decodedCredentials = Buffer.from(encodedCredentials, "base64").toString("utf-8")
        const [authUsername, authPassword] = decodedCredentials.split(":")
        isValid = await verifyAuthentication(authUsername, authPassword)
    }
    if(username !== undefined && password !== undefined) {
        isValid = await verifyAuthentication(username, password)
    }

    if(!isValid) return res.status(401).send({error: "Username or password is wrong"})

    next()
}

async function verifyAuthentication (username: string, password: string,) {

    if(username !== verifyUsername) {
        return false
    }

    const testPasswords = await argon2.verify(verifyhashPassword, password)
    if(!testPasswords) {
        return false
    }

    return true

}