import express from "express"
import nodeRouter from "./nodeRouter"
import secretNodeRouter from "./secretNodeRouter"

const server = express()
server.use(express.json())
server.use("/notes", nodeRouter)
server.use("/secretnodes", secretNodeRouter)

server.listen(3000, () => console.log("Listening to port", 3000))