1. node:latest -> 1 GB

2. node:slim -> 251.14 MB

3. node:alpine -> 180.32 MB

4. alpine:latest -> size 175.03 MB

I may prefer alpine or node alpine because their sizes are small.