import express, {Request, Response} from 'express'

const server = express()
server.use('/', express.static('./dist/client'))

server.get('*', (req: Request, res: Response) => {
    res.sendFile('index.html', { root: './dist/client' })
})


server.listen(3000, () => {
    console.log('Server listening port 3000')
})
