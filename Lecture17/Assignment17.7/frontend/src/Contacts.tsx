import { Outlet, Link } from 'react-router-dom'

export default function Contacts() {
    return <div className='Root'>
        <h1>Contacts</h1>
        <nav>
            <Link to='/contacts/1' > Keijo </Link>
            <Link to='/contacts/2' > Albertti </Link>
            <Link to='/contacts/3' > Sanna </Link>
        </nav>
        <Outlet  />
        
    </div>
}