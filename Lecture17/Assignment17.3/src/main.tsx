import React from 'react'
import ReactDOM from 'react-dom/client'

import { createBrowserRouter, RouterProvider } from 'react-router-dom'

const Root = () => {
  return <div className='Root'>This is Root</div>
}

const Contacts = () => {
  return <div className='Root'>
    <h1>Contacts</h1>
    </div>
}

const router = createBrowserRouter([
    {
        path: '/',
        element: <Root />,
    },
    {
      path: '/contacts',
      element: <Contacts />,
  }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)
