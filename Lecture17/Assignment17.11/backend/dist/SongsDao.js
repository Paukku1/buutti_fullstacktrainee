"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findSongById = exports.findSongs = void 0;
const db_1 = require("./db");
const findSongs = () => __awaiter(void 0, void 0, void 0, function* () {
    const query = 'SELECT id, title FROM "songlist"';
    const result = yield (0, db_1.executeQuery)(query);
    return result.rows;
});
exports.findSongs = findSongs;
const findSongById = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const query = 'SELECT * FROM "songlist" WHERE id = $1';
    const params = [id];
    const result = yield (0, db_1.executeQuery)(query, params);
    console.log(id);
    console.log(result.rows);
    return result.rows[0];
});
exports.findSongById = findSongById;
