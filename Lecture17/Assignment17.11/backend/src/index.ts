import express from 'express'
import SongRouter from './SongRouter'
import { createProductsTable } from './db'

createProductsTable()

const server = express()
server.use('/', express.static('./dist/client'))
server.use('/songs', SongRouter)

server.listen(3000, () => {
    console.log('Server listening port 3000')
})
