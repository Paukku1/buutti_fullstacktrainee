import express, { Request, Response } from 'express'
import { findSongs, findSongById } from './SongsDao'
const router = express.Router()

router.get('/', async (req: Request, res: Response) => {
    const result = await findSongs()
    res.send(result)
})

router.get('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await findSongById(id)
    res.send(result)
})
export default router