import {executeQuery} from './db'

export const findSongs = async() => {
    const query = 'SELECT id, title FROM "songlist"'
    const result = await executeQuery(query)
    return result.rows
}

export const findSongById = async(id: number) => {
    const query = 'SELECT * FROM "songlist" WHERE id = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log(id)
    console.log(result.rows)
    return result.rows[0]
}
