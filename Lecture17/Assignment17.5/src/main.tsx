import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Contacts from './Contacts'
import ContactChild, { loader as contactLoader }  from './ContactChild'

const Root = () => {
  return (
  <div className='Root'>This is root</div>)
}


const router = createBrowserRouter([
    {
        path: '/',
        element: <Root />,
    },
    {
      path: '/contacts/',
      element: <Contacts />,
      children: [
        {
          path: '/contacts/:id',
          element: <ContactChild />,
          loader: contactLoader
        }
      ]

  }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)
