import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Contacts from './Contacts'
import ContactChild, { loader as contactLoader }  from './ContactChild'
import ErrorPage from './ErrorPage'

const Root = () => {
  return (
  <div className='Root'>This is root</div>)
}


const router = createBrowserRouter([
    {
        path: '/',
        element: <Root />,
        errorElement: <ErrorPage />
    },
    {
      path: '/contacts/',
      element: <Contacts />,
      children: [
        {
          path: '/contacts/:id',
          element: <ContactChild />,
          loader: contactLoader
        }
      ],
      errorElement: <ErrorPage />

  }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)
