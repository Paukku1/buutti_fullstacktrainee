import { useState } from 'react'

function ChangeBoxColor() {
  const [bgColor, setBgColor] = useState('#9ED68F');
  
  const changeColor =()=>{
     const randomColor = "#" + ((1<<24)*Math.random() | 0).toString(16);
     setBgColor(randomColor);
   }
   const header = <h1 style={{textAlign:'center'}}>Assignment17.9</h1>

  return (
    <>
      {header}
      <div style={{
        backgroundColor: bgColor, 
        margin: 'auto',
        textAlign: 'center',
        padding: '70px 0'}}  onClick={changeColor} >
        <p>Click the box!</p>
        <p>{bgColor}</p>
      </div>
    </>
  )
}

export default ChangeBoxColor
