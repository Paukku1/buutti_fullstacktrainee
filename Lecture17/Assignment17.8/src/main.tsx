import React from 'react'
import ReactDOM from 'react-dom/client'
import ChangeBoxColor from './ChangeBoxColor.tsx'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <ChangeBoxColor />
  </React.StrictMode>,
)
