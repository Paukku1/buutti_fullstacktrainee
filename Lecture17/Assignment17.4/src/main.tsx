import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Contacts, { loader as contactLoader }  from './Contacts'

const Root = () => {
  return <div className='Root'>This is root</div>
}


const router = createBrowserRouter([
    {
        path: '/',
        element: <Root />,
    },
    {
      path: '/contacts/:id',
      element: <Contacts />,
      loader: contactLoader
  }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)
