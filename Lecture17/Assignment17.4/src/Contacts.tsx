import { useLoaderData } from 'react-router-dom'

interface Contact {
    id: number,
    name: string,
    phone: string,
    email: string
}


const array: Array<Contact> =  [
    {"id": 1, "name": "Keijo", "phone": "0401234567", "email": "keijo@gmail.com"}, 
    {"id": 2, "name": "Albertti", "phone": "0401234567", "email": "allu@gmail.com"},
    {"id": 3, "name": "Sanna", "phone": "040999111", "email": "sannamakela@gmail.com"}
]


export function loader({params}: any) {
    return params.id
}

export default function Contacts() {
    const id = useLoaderData() as number
    // vaihtoehtoinen ratkaisu:
    // const contact = array.find(c => c.id === id) ?? {name: '', phone: '', email: ''}

    return <div className='Root'>
        <h1>Contacts</h1>
    
        {array.filter(person => {
            return person.id === Number(id)}).map(person => {
                return (
                    <>
                        <h2>{person.name}</h2>
                        <p>{person.phone}</p>
                        <p>{person.email}</p>
                    </>
                )
        })}
    </div>
}