import express, { Request, Response } from 'express'
import songs from './songs'
const router = express.Router()

router.get('/', (req: Request, res: Response) => {

    const songList = songs.map(song => ({ id: song.id, title: song.title }))
    res.json(songList)
})

router.get('/:id', (req: Request, res: Response) => {
    const song = songs.find(song => song.id === parseInt(req.params.id))
    if (song) {
        res.json(song)
    } else {
        res.status(404).json({ error: 'Song not found' })
    }
})


export default router