import { useState, useEffect } from 'react'
import { useLoaderData } from 'react-router-dom'
import axios from 'axios'

interface Song {
    id: number,
    title: string,
    lyrics: string,
}

export function loader({params}: any) {
    const id = Number(params.id)
    if (id === 1 || id === 2 || id === 3 || id === 4 || id === 5) {
        return id
    }
    throw new Response('Wrong id', {status: 404})
    
}

function Songs () {
    const [songList, setSongList] = useState<Song[]>([])

    useEffect(() => {
        // Fetch the song list from the backend on startup
        const fetchSongList = async () => {
          console.log('hei')
          try {
            const response = await axios.get('/songs/1');
            setSongList(response.data);
          } catch (error) {
            console.error(error);
          }
        }
    
        fetchSongList();
      }, []);

    const id = useLoaderData() as number
    const findSong = songList.find((song: Song) => song.id === id) ?? { title: '', lyrics: '' }
    
    return(
        <>
            <h2>{findSong.title}</h2>
            <p>{findSong.lyrics}</p>
        </>
    )
}

export default Songs