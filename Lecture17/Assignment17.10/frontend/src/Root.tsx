import { Link, Outlet } from 'react-router-dom'
import songs from './songlist'

function Root () {
    const header = <h2 style={{textAlign:'center'}}>Songs</h2>

    const songList = () => {
        return (
            <>
            <nav>
            {songs.map(song => {
               return <Link to={'/' + song.id}>{song.title}</Link>
            })}
            </nav>
            

            <Outlet />
            </>
        )
    }
    return (
        <> 
            {header}
            {songList()}
        </>
    )
}

export default Root