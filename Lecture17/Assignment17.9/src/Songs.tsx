import { useLoaderData } from 'react-router-dom'
import songs from './songlist'

interface Song {
    id: number,
    title: string,
    lyrics: string,
}


export function loader({params}: any) {
    const id = Number(params.id)
    if (id === 1 || id === 2 || id === 3 || id === 4 || id === 5) {
        return id
    }
    throw new Response('Wrong id', {status: 404})
    
}

function Songs () {
    const id = useLoaderData() as number
    const findSong = songs.find((song: Song) => song.id === id) ?? { title: '', lyrics: '' }
    console.log(findSong)
    return(
        <>
            <h2>{findSong.title}</h2>
            <p>{findSong.lyrics}</p>
        </>
    )
}

export default Songs