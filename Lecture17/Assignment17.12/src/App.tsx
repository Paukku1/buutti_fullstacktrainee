import { useState, useEffect, MouseEvent } from 'react';

const DraggableBox = () => {
  const [position, setPosition] = useState({ x: 0, y: 0 })
  const [isDragging, setIsDragging] = useState(false)
  const [dragStart, setDragStart] = useState({ x: 0, y: 0 })

  useEffect(() => {
    const centerX = window.innerWidth / 2 - 350; 
    const centerY = window.innerHeight / 2 - 180; 

    setPosition({ x: centerX, y: centerY });
  }, []);

  const handleMouseDown = (event: MouseEvent) => {
    const {clientX, clientY} = event
    setDragStart({x: clientX, y:clientY})
    setIsDragging(true)
  }

  const handleMouseUp = () => {
    setIsDragging(false)
  }

  const handleMouseMove = (event: MouseEvent) => {
    if (!isDragging) return

    const { clientX, clientY } = event;
    const offsetX = clientX - dragStart.x;
    const offsetY = clientY - dragStart.y;

    setPosition((prevPosition) => ({
      x: prevPosition.x + offsetX,
      y: prevPosition.y + offsetY,
    }))

    setDragStart({ x: clientX, y: clientY });
  }

  return (
    <div
      style={{
        position: 'absolute',
        top: position.y,
        left: position.x,
        width: '200px',
        backgroundColor: '#112233',
        color: '#fefefe',
        fontSize: '20px',
        fontWeight: 'bold',
        textAlign: 'center',
        padding: '50px 0'
      }}
      onMouseDown={handleMouseDown}
      onMouseUp={handleMouseUp}
      onMouseMove={handleMouseMove}
    >
      Drag me!
    </div>
  )
}

export default DraggableBox;