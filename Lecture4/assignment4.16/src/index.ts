const start = parseInt(process.argv[2])
const end = parseInt(process.argv[3])

const listNumbers = (start: number, end: number) => {
    const listOfNumbers: Array<number> = []
    if(start < end) {
        for(let i = start; i <= end; i++) {
            listOfNumbers.push(i)
        }
        return listOfNumbers
    }
    else {
        for(let j = start; j >= end; j--) {
            listOfNumbers.push(j)
        }
        return listOfNumbers
    }
}

// Kannattaa käyttää for-loopeissa omia indeksejä sen sijaan että muuttaa funktiolle annettuja parametreja. Tässä esimerkissä ei ole mitään väliä, mutta monesti voi aiheuttaa kummia bugeja muuttamalla parametreja, jotka on käytössä muualla ohjelmassa.

console.log(listNumbers(start, end))