const manipulate = () => {
    const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]
    const arrForThree = arr.filter(num => num % 3 === 0)
    console.log(arrForThree)

    const arrMultTwo = arr.map(num => num*2)
    console.log(arrMultTwo)

    const sum = arr.reduce((acc, cur) => {
        return acc + cur
    }, 0) 

    console.log(sum)
}

manipulate()