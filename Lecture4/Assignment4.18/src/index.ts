const str = process.argv[2]

const reversedWords = (str: string) => { 
    const reversedStr = str.split(/(\s|,|\.)/).map(item => {    
        return item.split("").reverse().join("")
    }).join("")

    if(str === reversedStr){
        return "Yes " + str + " is palindrome"
    }
    else {
        return "No " + str + " is not palindrome"
    }
}

console.log(reversedWords(str))

// korjattu muuttujan nimeä
// reverseWords?