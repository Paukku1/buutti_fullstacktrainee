const fibonacciSequence = (n: number) => {
    const array: Array<number> = [0, 1]

    for(let i = 2; i < n; i++) {
        array[i] = array[i-2]+array[i-1]
    }
    return array
}

console.log(fibonacciSequence(8))