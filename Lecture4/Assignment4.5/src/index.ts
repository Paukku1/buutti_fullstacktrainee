const calculate = (callback: (a: number) => void) => 
{
    let result = 0
    for(let i=0; i < 1_000_000_000; i++) {
        if(i % 3 === 0 && i % 5 === 0 && i % 7 === 0) {
            result = result + i
        }
    }
    callback (result)
}

function logger(something: number){
    console.log(something)
}

calculate(logger)
