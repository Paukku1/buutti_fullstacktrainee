"use strict";
const calculate = (callback) => {
    let result = 0;
    for (let i = 0; i < 1000000000; i++) {
        if (i % 3 === 0 && i % 5 === 0 && i % 7 === 0) {
            result = result + i;
        }
    }
    callback(result);
};
function logger(something) {
    console.log(something);
}
calculate(logger);
