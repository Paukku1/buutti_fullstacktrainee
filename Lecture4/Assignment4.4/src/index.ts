function joinator(stringit: string[], stringi: string) {
    const newString = stringit.reduce((accumulator, element, index) => {
        if (index < stringit.length - 1) {
            return accumulator + element + stringi
        } else return accumulator + element
    }, '')
    return newString
}

console.log(joinator(['tarzan', 'monkees', 'friends'], 'jane'))