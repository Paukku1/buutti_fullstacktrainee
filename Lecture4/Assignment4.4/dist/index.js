"use strict";
const dicegeneratorfunction = (sides) => {
    const diceFunction = () => {
        return Math.floor(Math.random() * sides) + 1;
    };
    return diceFunction;
};
const sidesSix = dicegeneratorfunction(6);
const sidesEight = dicegeneratorfunction(8);
console.log(sidesSix() + sidesSix() + sidesEight() + sidesEight());
