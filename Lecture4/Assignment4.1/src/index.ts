function uppercase (str: string) {
    const words = str.split(' ')
    for(let i = 0; i < words.length; i++) {
        words[i] = words[i][0].toUpperCase() + words[i].substring(1)
    }
    console.log(words.join(' '))
}

uppercase('this is some random string')
