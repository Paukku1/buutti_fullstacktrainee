const num = parseInt(process.argv[2])

const primeNumber = (num: number) => {
    if(num <= 2) {
        return "This " + num + " is not prime number"
    }
    else {
        for(let i = 2; i <= num - 1; i++) {
            if(num % i === 0) {
                return num + " is not prime number"
            }
            else {
                return "The number " + num + " is prime number!"
            }
        }
    }
}

console.log(primeNumber(num))

// Näähän on melkein mallivastauksia kaikki. Laitoin muutamia clean code-kommentteja ja ajatuksia siitä mihin kannattaa ajattelua ja koodaustapoja kehittää, mutta näyttää siltä että oot tosi hyvällä mallilla.