"use strict";
const num = parseInt(process.argv[2]);
const primeNumber = (num) => {
    if (num <= 2) {
        return "This " + num + " is not prime number";
    }
    else {
        for (let i = 2; i <= num - 1; i++) {
            if (num % i === 0) {
                return num + " is not prime number";
            }
            else {
                return "The number " + num + " is prime number!";
            }
        }
    }
};
console.log(primeNumber(num));
