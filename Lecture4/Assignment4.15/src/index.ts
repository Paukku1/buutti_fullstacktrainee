const printCompetitors = () => {
    const competitors = ["Julia", "Mark", "Spencer", "Ann", "John", "Joe"]
    const ordinals = ["st", "nd", "rd", "th"]
    const list: Array<string>  = []
    
    competitors.forEach((name, index) => {
        let oIndex = ""
        if((index + 1) <= 3){
            oIndex = ordinals[index]
        }
        else {
            oIndex = ordinals[ordinals.length-1]
        }

        list.push((index +1) + oIndex + " competitor was " + name)
    })
    return list
}

// Korjattu if else rakenteesta list.pushit pois:
// Tässä ei varsinaisesti ole mitään vikaa, mutta koodissa on nyt melkein sama asia kahteen kertaan (toistoa). 
// Tästä pääsisi määrittelemällä ordinalsin indeksin muuttujaan, joka on riippuvainen index-muuttujan arvosta.

console.log(printCompetitors())