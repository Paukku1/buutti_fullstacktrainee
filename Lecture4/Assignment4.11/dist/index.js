"use strict";
const list = ['Hello', 'world', 'I', 'use', 'TypeScript'];
const join = (arr, separator) => {
    return arr.reduce((acc, el) => acc + el + separator, '').slice(0, -1);
};
console.log(join(list, ' '));
