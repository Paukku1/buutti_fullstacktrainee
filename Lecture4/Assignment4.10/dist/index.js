"use strict";
const names = [
    'Murphy',
    'Hayden',
    'Parker',
    'Arden',
    'George',
    'Andie',
    'Ray',
    'Storm',
    'Tyler',
    'Pat',
    'Keegan',
    'Carroll'
];
const findName = names.find(name => name.length === 3 && name.endsWith('t'));
console.log(findName);
