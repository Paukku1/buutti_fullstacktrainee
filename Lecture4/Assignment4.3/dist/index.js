"use strict";
function namedfun(a, b) {
    return a + b;
}
const anofun = function (a, b) {
    return a - b;
};
const arrowfun = (a, b) => {
    return a * b;
};
console.log(namedfun(2, 3));
console.log(anofun(5, 3));
console.log(arrowfun(4, 3));
