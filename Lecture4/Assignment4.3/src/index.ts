function namedfun (a: number, b:number) {
    return a+b
}

const anofun = function (a: number, b:number) {
    return a+b
}

const arrowfun = (a: number, b: number) => {
    return a+b
} 

console.log(namedfun(2, 3))
console.log(anofun(5, 3))
console.log(arrowfun(4, 3))