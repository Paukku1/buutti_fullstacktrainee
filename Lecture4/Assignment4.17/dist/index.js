"use strict";
const str = process.argv[2];
const reverseWords = (str) => {
    return str.split(/(\s|,|\.)/).map(item => {
        return item.split("").reverse().join("");
    }).join("");
};
console.log(reverseWords(str));
