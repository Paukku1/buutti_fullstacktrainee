"use strict";
const randomArray = () => {
    const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
    let i = array.length;
    let index = 0;
    let t = 0;
    while (i >= 1) {
        index = Math.floor(Math.random() * i--);
        t = array[i];
        array[i] = array[index];
        array[index] = t;
    }
    return array;
};
console.log(randomArray());
// Korjattu? While looppi
// Välttäisin nollaa totuusarvona. Sillä saa helposti bugeja, koska 0 on täysin validi indeksi arraylle, mutta silti falsy arvo.
