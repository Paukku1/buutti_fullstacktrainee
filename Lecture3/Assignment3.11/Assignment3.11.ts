const a = process.argv[2]
const b = process.argv[3]
const c = process.argv[4]

if(a < b) {
  console.log(b, "is greater")
}
else if(a > b) {
  console.log(a, "is greater")
}
else if(a === b) { // tässä ei tarvitse enää tehdä vertailua, sillä muut vaihtoehdot on jo käyty läpi, ja on hyvä että moniportaisen if/else lauseen lopussa on "catch all" else-haara.
  console.log("they are equal")
}

if(c) {
    // Tähän haaraan tullaan nyt, mikäli c:n arvo on truthy, eli jos siellä on mikä tahansa string.
    // Salasanaa ei siis "tarkisteta" missään vaiheessa. 
  console.log("Yay, you guessed the password")
  if(a === b) {
    console.log("Hello World")
  }
}