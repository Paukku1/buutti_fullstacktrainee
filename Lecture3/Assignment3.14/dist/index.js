"use strict";
const balance = 0;
const isActive = true;
const checkBalance = true;
const atm = (balance, isActive, checkBalance) => {
    if (!checkBalance)
        return ("have a nice day!");
    else {
        if (!isActive) {
            return "Your account is not active";
        }
        else if (balance <= 0) {
            return "Your account is empty";
        }
        else {
            return balance;
        }
    }
};
console.log(atm(balance, isActive, checkBalance));
