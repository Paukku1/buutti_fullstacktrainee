const atm = (balance: number, isActive: boolean, checkBalance: boolean) => {
    if(!checkBalance) return ("have a nice day!")
    else {
        if(!isActive) {
            return "Your account is not active"
        }
        else if(balance <= 0) {
            return "Your account is empty"
        }
        else {
            return balance
        }
    }
}
console.log(atm(300, true, true))