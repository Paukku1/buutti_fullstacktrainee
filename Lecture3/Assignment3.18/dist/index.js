"use strict";
const str = process.argv[2];
const repch = process.argv[3];
const toch = process.argv[4];
const replaceChar = (str, repch, toch) => {
    const re = new RegExp("" + repch, "g");
    console.log(re);
    const result = str.replace(re, toch);
    return result;
};
console.log(replaceChar(str, repch, toch));
