const str = process.argv[2]
const repch = process.argv[3]
const toch = process.argv[4]

const replaceChar = (str: string, repch: string, toch: string) => {
    const re = new RegExp("" + repch, "g")
    const result = str.replace(re, toch)
    // regexejä! next level.
    // muista myös, että on olemassa .replaceAll() metodi
    return result
}

console.log(replaceChar(str, repch, toch))


// Hyvin menneet tehtävät. Katso korjaukset läpi ja tsekkaa että ymmärrät mistä niissä on kysymys. Sitten vaan keep up the good work!