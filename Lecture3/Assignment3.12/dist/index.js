"use strict";
const number1 = parseInt(process.argv[2]);
const number2 = parseInt(process.argv[3]);
const number3 = parseInt(process.argv[4]);
const findNumber = (number1, number2, number3) => {
    if (number1 === number2 && number2 === number3) {
        return ('All are equal');
    }
    else {
        const list = [];
        list.push(number1, number2, number3);
        const largest = Math.max(...list);
        const smallest = Math.min(...list);
        return ('Largest is number ' + largest + ' and smallest is ' + smallest);
    }
};
const returned = findNumber(number1, number2, number3);
console.log(returned);
