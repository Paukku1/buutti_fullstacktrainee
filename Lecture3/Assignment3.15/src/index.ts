const name1 = process.argv[2]
const name2 = process.argv[3]
const name3 = process.argv[4]

const initialLetters = (name1: string, name2: string, name3: string) => {
    let list = []
    list.push(name1, name2, name3)
    
    for(let i = 0; i < list.length; i++) {
        list[i] = list[i].charAt(0)
    }

    return list.join('.')
}

const lengthComparison = (name1: string, name2: string, name3: string) => {
    let list = []
    list.push(name1, name2, name3)
    list.sort(function(a, b){return b.length - a.length})
    return list.join(' ')
}

console.log(initialLetters(name1, name2, name3))
console.log(lengthComparison(name1, name2, name3))