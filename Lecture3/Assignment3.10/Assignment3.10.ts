const language = process.argv[2]

// .toLowerCase() kannattaa tehdä ennen vertailuja, ja tallettaa tulos muuttujaan, niin muunnosta ei tarvitse tehdä jokaisessa if/else haarassa uudestaan

if (language.toLowerCase() === "en") {
  console.log("Hello World!")
}
else if (language.toLowerCase() === "fi") {
  console.log("Tervehdys maailma!")
}
else if (language.toLowerCase() === "es") {
  console.log("Hola mundo!")
}
else {
  console.log("Hello world")
}
