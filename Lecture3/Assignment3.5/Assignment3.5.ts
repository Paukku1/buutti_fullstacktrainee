let arr = ['banaani', 'omena', 'mandariini', 'appelsiini', 'kurkku', 'tomaatti', 'peruna']

console.log(arr[2], arr[4], arr.length)

arr.sort((a, b) => {
    return a.localeCompare(b, 'en')
})
// .sort() default arvo on aakkostaa, joten peruskäytössä riittää vain arr.sort()

console.log(arr)

arr.push('sipuli')
console.log(arr)

arr.shift()
arr.forEach(element => {
    console.log(element)
})
console.log('')

arr.forEach(element => {
    if(element.includes('r')) {
        console.log(element) }
})