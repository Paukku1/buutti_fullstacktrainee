const lowOrUp = process.argv[2]
const string = process.argv[3]

const lowUp = (lowOrUp: string, str: string) => {
    if(lowOrUp.toLowerCase() === 'lower') {
        return str.toLowerCase()
    }
    else if(lowOrUp.toLowerCase() === 'upper') {
        return str.toUpperCase()
    }
    else {
        return "you did not pick lower or upper letters"
    }
}

let returnedstr = lowUp(lowOrUp, string)
console.log(returnedstr)