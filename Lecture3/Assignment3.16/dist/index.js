"use strict";
const lowOrUp = process.argv[2];
const string = process.argv[3];
const lowUp = (lowOrUp, str) => {
    if (lowOrUp.toLowerCase() === 'lower') {
        let retstring = str.toLowerCase();
        return retstring;
    }
    else if (lowOrUp.toLowerCase() === 'upper') {
        str = str.toUpperCase();
        return (str);
    }
    else {
        return "you did not pick lower or upper letters";
    }
};
let returnedstr = lowUp(lowOrUp, string);
console.log(returnedstr);
