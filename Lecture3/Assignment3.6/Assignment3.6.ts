// 3.6.1
let sumstring = ""
for(let i=0; i<=1000; i = i + 100) {
    sumstring = sumstring + `${i} `
}
console.log(sumstring.trim())

// 3.6.2
sumstring = ""
for(let i=1; i < 129;  i = i*2) {
    sumstring = sumstring + `${i} `
}
console.log(sumstring.trim())

// 3.6.3
sumstring = ""
for(let i=1; i < 6; i = i+1) {
    // suositaan i++ käyttöä for-loopissa, koska se on yleinen käytäntö
    // muualla voi käyttää sit mitä haluaa
    sumstring = sumstring + `${i*3} `
}
console.log(sumstring.trim())

// 3.6.4
sumstring = ""
for(let i = 9; i >= 0; i = i-1) {
    // ja i-- vastaavasti
    sumstring = sumstring + `${i} `
}
console.log(sumstring.trim())

// 3.6.5
sumstring = ""
for(let i = 1; i < 5; i = i+1) {
    sumstring = sumstring + `${i} ${i} ${i} `
}
console.log(sumstring.trim())

// 3.6.6
sumstring = ""
for(let i = 0; i < 4; i++) {
    for (let index = 0; index < 5; index++) {
    sumstring = sumstring + `${index} `
    }
}
console.log(sumstring.trim())