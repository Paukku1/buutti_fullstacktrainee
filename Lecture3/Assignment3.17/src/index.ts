const str = process.argv[2]

const dropOut = (str: string) => {
    const lastIndex = str.lastIndexOf(" ") // Hyvin käytetty .lastIndexOf metodia!
    str = str.substring(0, lastIndex)
    return str
}

console.log(dropOut(str))