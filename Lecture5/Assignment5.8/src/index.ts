import fs from 'fs'

const read = fs.readFileSync('forecast_data.json', 'utf-8')
const data = JSON.parse(read)

data.temperature = 18

const jsonString = JSON.stringify(data)
fs.writeFileSync('./forecast_data.json', jsonString, 'utf-8')