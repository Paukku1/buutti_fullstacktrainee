"use strict";
const collatzConjecture = (n) => {
    let steps = 0;
    while (n !== 1) {
        if (n % 2 === 0) {
            n = n / 2;
        }
        else {
            n = n * 3 + 1;
        }
        steps++;
    }
    return steps;
};
const step = collatzConjecture(3);
console.log(`It takes ${step} steps to reach number 1`);
