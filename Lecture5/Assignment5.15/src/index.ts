const collatzConjecture = (n: number) => {
    let steps = 0
    while (n !== 1) {
        if(n % 2 === 0) {
            n = n / 2
        }
        else {
            n = n * 3 + 1
        }
        // Ehdotan tähän ternääriä
        // n = n % 2 === 0  ? n / 2 : n * 3 + 1
        steps++
    }

    return steps
}

const step = collatzConjecture(3)

console.log(`It takes ${step} steps to reach number 1`)

// Hyvältä näyttää. Ei isoja ongelmia missään, paljon hyviä ratkaisuja.