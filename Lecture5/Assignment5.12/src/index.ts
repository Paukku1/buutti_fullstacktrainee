const getVowelCount = (str: string) => { 
    const char = str.toLowerCase().split('')
    const vowelsArray = ['a','o','i','u','y']
    const result = char.filter(letter => vowelsArray.includes(letter)) // hyvää filterin käyttöä
    return result.length    
}

console.log(getVowelCount('abracadabra')) // 5
