"use strict";
const getVowelCount = (str) => {
    const char = str.toLowerCase().split('');
    const vowelsArray = ['a', 'o', 'i', 'u', 'y'];
    const result = char.filter(letter => vowelsArray.includes(letter));
    return result.length;
};
console.log(getVowelCount('abracadabra')); // 5
