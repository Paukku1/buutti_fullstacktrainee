"use strict";
function likes(likearr) {
    let str = '';
    if (likearr.length < 1) {
        return 'no one likes this';
    }
    else if (likearr.length < 2) {
        return likearr[0] + ' likes this';
    }
    else if (likearr.length < 3) {
        for (let i = 0; i < likearr.length - 1; i++) {
            str = str + likearr[i] + ' ';
        }
        str = str + 'and ' + likearr[likearr.length - 1] + ' like this';
        return str;
    }
    else if (likearr.length < 4) {
        for (let i = 0; i < likearr.length - 1; i++) {
            str = str + likearr[i] + ', ';
        }
        str = str + 'and ' + likearr[likearr.length - 1] + ' like this';
        return str;
    }
    else {
        for (let i = 0; i < 2; i++) {
            str = str + likearr[i] + ', ';
        }
        str = str + 'and ' + (likearr.length - 2) + ' other like this';
        return str;
    }
}
console.log(likes([])); // "no one likes this"
console.log(likes(['John'])); // "John likes this"
console.log(likes(['Mary', 'Alex'])); // "Mary and Alex like this"
console.log(likes(['John', 'James', 'Linda'])); // "John, James and Linda like this"
console.log(likes(['Alex', 'Linda', 'Mark', 'Max']));
console.log(likes(['Alex', 'Linda', 'Mark', 'Max', 'Elliot']));
