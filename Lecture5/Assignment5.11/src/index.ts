const aboveAverage = (arr:Array<number>) => {
    const arr2: Array<number> = []
    
    if(arr.length % 2 === 0) {
        arr2.push(arr[(arr.length/2)-1])
        arr2.push(arr[arr.length/2])
        return arr2
    }
    else {
        arr2.push(arr[Math.floor(arr.length/2)])
        return arr2
    }

}
console.log(aboveAverage([1, 5, 9, 3]))
console.log(aboveAverage([1, 5, 9, 3, 7]))

// Tää ei nyt taida noudattaa tehtävänantoa. Tarkoitus oli, että funktio palauttaa ne arvot, jotka listassa on korkeampia kuin listan keskiarvo.