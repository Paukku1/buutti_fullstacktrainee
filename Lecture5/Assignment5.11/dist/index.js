"use strict";
const aboveAverage = (arr) => {
    const arr2 = [];
    if (arr.length % 2 === 0) {
        arr2.push(arr[(arr.length / 2) - 1]);
        arr2.push(arr[arr.length / 2]);
        return arr2;
    }
    else {
        arr2.push(arr[Math.floor(arr.length / 2)]);
        return arr2;
    }
};
console.log(aboveAverage([1, 5, 9, 3]));
console.log(aboveAverage([1, 5, 9, 3, 7]));
