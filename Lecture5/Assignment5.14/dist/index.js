"use strict";
const generateCredentials = (name, lastname) => {
    const username = generateUsername(name, lastname);
    const password = generatePassword(name, lastname);
    const array = [username, password];
    return array;
};
const generateUsername = (name, lastname) => {
    let uName = 'B';
    const date = new Date();
    const digits = date.getFullYear().toString().substr(-2);
    uName = uName + digits + lastname.substr(0, 2).toLowerCase() + name.substr(0, 2).toLowerCase();
    return uName;
};
const generatePassword = (name, lastname) => {
    let password = '';
    const randomChar = Math.floor(Math.random() * 90) + 65;
    const specialChar = Math.floor(Math.random() * 47) + 33;
    const date = new Date();
    const digits = date.getFullYear().toString().substr(-2);
    password = String.fromCharCode(randomChar) + name.substr(0, 1).toLowerCase() + lastname.substr(-1, 1).toUpperCase() + String.fromCharCode(specialChar) + digits;
    return password;
};
console.log(generateCredentials('John', 'Doe')); // [ 'B22dojo', 'KjE,22' ]
