function calculator(operator: string, num1: number, num2: number) {
    if (operator === '+') {
        return num1 + num2
    } else if (operator === '-') {
        return num1 - num2
    } else if (operator === '*') {
        return num1 * num2
    } else if (operator === '/') {
        return num1 / num2
    } else {
        return 'Can not do that!'
    }
    // Mä formatoisin nää vielä tällai JS-orientoituneesti alkamaan edellisen aaltosulkeen perään
}

console.log(calculator('+', 1, 2))