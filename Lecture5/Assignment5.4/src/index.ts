class Ingredient {
    name: string
    amount: number
    constructor(name: string, amount: number) {
        this.name = name
        this.amount = amount
    }
    scale (factor: number) {
        this.amount = factor * this.amount
    }
}

class Recipe {
    name: string
    ingredients: Array<{ name: string, amount: number }>
    servings: number  
    constructor(name: string, ingredients: Array<{ name: string, amount: number }>, servings: number) { // Koska meillä on class käytössä, se tarjoaa tyypityksen samalla, voit siis käyttää Array<Ingredient>
        this.name = name
        this.ingredients = ingredients
        this.servings = servings
    }
    toString () {
        return this.ingredients.reduce((acc: string, cur: { name: string, amount: number }) => {
            return acc + `- ${cur.name} (${cur.amount})\n`
        }, `${this.name} (${this.servings} servings)\n\n`)
    }

}

const flour = new Ingredient("flour", 300)
const water = new Ingredient("water", 150)
const oil = new Ingredient("Oil", 30)
const salt = new Ingredient("Salt", 0)

const tortillas = new Recipe("tortillas", [flour, water, oil, salt], 12)

console.log(tortillas.toString())