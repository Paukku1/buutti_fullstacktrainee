const checkExam = (arr1: Array<string>, arr2: Array<string>) => {
    let points = 0
    for(let i = 0; i < arr1.length; i++) {
        if (arr1[i] === arr2[i]) {
            points = points + 4
        }
        else if (arr1[i] !== arr2[i] && arr2[i] !== '' && points > 0) { // tää logiikka antaa itse asiassa eri tuloksen riippuen siitä missä kohtaa tenttiä virheet on
            points = points -1
        }
    }
    return points
}

console.log(checkExam(['a', 'a', 'b', 'b'], ['a', 'c', 'b', 'd'])) // 6  
console.log(checkExam(['a', 'a', 'c', 'b'], ['a', 'a', 'b',  ''])) // 7  
console.log(checkExam(['a', 'a', 'b', 'c'], ['a', 'a', 'b', 'c'])) // 16  
console.log(checkExam(['b', 'c', 'b', 'a'], ['',  'a', 'a', 'c'])) // 0 
