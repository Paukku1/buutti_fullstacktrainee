"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
let read = fs_1.default.readFileSync('./src/laulu.txt', 'utf-8');
read = read.replace(/joulu/gi, 'kinkku');
read = read.replace(/lapsilla/gi, 'poroilla');
fs_1.default.writeFileSync('./src/newSong.txt', read, 'utf-8');
