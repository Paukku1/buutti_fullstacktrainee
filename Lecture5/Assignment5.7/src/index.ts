import fs from 'fs'

let read = fs.readFileSync('laulu.txt', 'utf-8')

read = read.replace(/joulu/gi, 'kinkku')
read = read.replace(/lapsilla/gi, 'poroilla')

fs.writeFileSync('newSong.txt', read, 'utf-8')