import express from 'express'
import { createProductsTable } from './db'
import router from './router'

export const server = express()
server.use(express.json())

server.use('/products', router)

const { PORT } = process.env
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT)
})
