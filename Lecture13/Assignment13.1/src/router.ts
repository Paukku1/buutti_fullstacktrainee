import express, { Request, Response } from "express"
import {addProduction, findProducts, findProduct, updatingProduct, deleteProduct} from './dao'

const router  = express.Router()

router.post('/', async (req: Request, res: Response) => {
    const {name, price} = req.body
    await addProduction(name, price)
    res.send('OK')
})

router.get('/', async (req: Request, res: Response) => {
    const result = await findProducts()
    res.json(result)
})

router.get('/:id', async (req: Request, res: Response) => {
    const id = parseInt(req.params.id)
    const result = await findProduct(id)
    res.json(result)
})

router.put('/:id', async(req: Request, res: Response) => {
    const id = parseInt(req.params.id)
    const {name, price} = req.body
    const result = await updatingProduct(id, name, price)
    res.send(result)
})

router.delete('/:id', async(req: Request, res: Response) => {
    const id = parseInt(req.params.id)
    await deleteProduct(id)
    res.json(`Delete: ${id}`)
})

export default router