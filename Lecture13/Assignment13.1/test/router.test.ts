import { jest } from '@jest/globals'
import request from 'supertest'
import { pool } from '../src/db'
import {server} from '../src/index'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /products', () => {
    const mockResponse = {
        rows: [
            { id: 101, name: 'Test Item 1', price: 100 },
            { id: 102, name: 'Test Item 2', price: 200 }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test get all', async () => {
        const response = await request(server).get('/products')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)

        expect(true).toBe(true)
    })

})

describe('Testing GET /products/:id', () => {
    const mockResponse = {
        rows: [
            { id: 101, name: 'Test Item 1', price: 100 }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test get id', async () => {
        const response = await request(server).get('/products/101')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows[0])

        expect(true).toBe(true)
    })

})

describe('Testing DELETE /products/:id', () => {
    const mockResponse = {
        rows: [
            { id: 101, name: 'Test Item 1', price: 100 }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test delete id', async () => {
        const response = await request(server).delete('/products/101')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('Delete: 101')

        expect(true).toBe(true)
    })

})
