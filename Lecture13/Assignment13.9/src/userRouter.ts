import express, { Request, Response } from 'express'
import { addUser, findUsers, findUserById, deleteUser, changeUser } from './userDao'
import { checkUserPostValues, checkValidUsername, checkUserId, checkPutValues } from './middleware'

const router  = express.Router()

router.get('/', async (req: Request, res: Response) => {
    const result = await findUsers()
    res.send(result)
})

router.get('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await findUserById(id)
    res.send(result)
})

router.post('/', async (req: Request, res: Response) => {
    if(req.body === undefined) return res.status(401).json({error: 'no req.body'})
    const {username, fullname, email} = req.body
    const result = checkUserPostValues(username, fullname, email)
    if(!result) return res.status(401).json('Missing params. Added username, fullname and email?')

    const checkusername = await checkValidUsername(username)
    if(checkusername) return res.status(401).json({error: 'Username is already exist'})

    await addUser(username, fullname, email)
    res.json('Added new user')
})

router.put('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await findUserById(id)
    if(!result) return res.status(401).send({error: 'User did not found'})
    
    if(req.body === undefined) return res.status(401).send({error: 'req.body missing'})
    const {username, fullname, email} = req.body
    
    const validInput = await checkPutValues([username, fullname, email])
    if(!validInput) return res.status(401).send({error: 'change username, fullname or email'})

    await changeUser(id, username, fullname, email)

    res.status(200).json('Change value(s)')

})

router.delete('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const checkuser = await checkUserId(id)
    if(checkuser === undefined) return res.status(404).send({error: 'Did not find that username in database'})

    await deleteUser(id)

    res.status(200).json(`Delete user ${id}`)
})


export default router