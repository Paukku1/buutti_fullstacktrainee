CREATE DATABASE "products_api";

CREATE TABLE "products" (
	"id" SERIAL PRIMARY KEY,
	"name" varchar,
	"price" real
);

INSERT INTO "products" (name, price)
VALUES ("Kani", 200)