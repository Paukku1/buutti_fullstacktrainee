import {executeQuery} from './db'



// check POST from userrouter, postsrouter and commentsrouter
export function checkUserPostValues (username: string, fullname: string, email: string) {
    let allValues = true
    if(username === undefined || username === '') return allValues = false 
    if(fullname === undefined|| fullname === '') return allValues = false 
    if(email === undefined || email === '') return allValues = false
    
    return allValues
}

export async function checkPostPostValues (user: number, title: string, content: string) {
    let allValues = true

    const query = 'SELECT * FROM users'
    const result = await executeQuery(query)
    const findUser = result.rows.find(dbuser => dbuser.id === user)
    
    if(findUser === undefined) return allValues = false 
    if(title === undefined || title === '') return allValues = false 
    if(content === undefined || content === '') return allValues = false
    
    return allValues
}

export async function checkPostCommentValues (id: number, post_id: number, content: string) {
    let allValues = true

    const query = 'SELECT * FROM "users"'
    const result = await executeQuery(query)
    const findUser = result.rows.find(user => user.id === id)
    
    const querypost = 'SELECT * FROM posts'
    const resultpost = await executeQuery(querypost)
    const findPostId = resultpost.rows.find(post => post.id === post_id)

    if(findUser === undefined) return allValues = false 
    if(findPostId === undefined) return allValues = false 
    if(content === undefined || content === '') return allValues = false
    
    return allValues
}

// Check username for post
export async function checkValidUsername (username: string) {
    const query = 'SELECT * FROM "users"'
    const result = await executeQuery(query)
    const findusername = result.rows.find(user => user.username === username)

    return findusername
}

// Check username for comments get by userid
export async function checkCommentByUser (id: number) {
    const query = 'SELECT * FROM "comments"'
    const result = await executeQuery(query)
    const findUser = result.rows.find(user => user.id === id)
        
    return findUser
}

// Check ids for delete. checkPostId delete and post

export async function checkCommentId (id: number) {
    const query = 'SELECT * FROM comments'
    const result = await executeQuery(query)
    const findId = result.rows.find(comment => comment.id === id)

    return findId
}

export async function checkUserId (id: number) {
    const query = 'SELECT * FROM users'
    const result = await executeQuery(query)
    const findUser = result.rows.find(user => user.id === id)
    
    return findUser
}

export async function checkPostId (id: number) {
    const query = 'SELECT * FROM posts'
    const result = await executeQuery(query)
    const findId = result.rows.find(post => post.id === id)
    return findId
}

// Check is there values what user want change 

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function checkPutValues(values: Array<any>) {
    let noEmptyValue = false
    
    values.forEach(value => {
        if(value !== undefined) {
            noEmptyValue = true
            return noEmptyValue
        }
    })
    return noEmptyValue
}