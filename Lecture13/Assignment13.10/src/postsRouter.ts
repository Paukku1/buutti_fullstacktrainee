import express, {Request, Response} from 'express'
import { getAllPosts, getPostById, postNewPost, deletePostById, changePostValues } from './postsDao'
import { checkPostPostValues, checkPostId, checkPutValues, checkUserId, checkCommentId } from './middleware'

const router = express.Router()

router.get('/', async (req:Request, res: Response) => {
    const result = await getAllPosts()

    res.status(200).json(result)
})

router.get('/:id',async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const checkId = await checkPostId(id)
    if(checkId === undefined) return res.status(404).json({error: 'Id did not found'})

    const result = await getPostById(id)
    res.status(200).json(result)
})

router.post('/',async (req: Request, res: Response) => {
    const {user, title, content} = req.body
    const checkValues = await checkPostPostValues(user, title, content)

    if(!checkValues) return res.status(401).json({error: 'Missing value: Values that needed: user, title and content. Or wrong user'})

    await postNewPost(user, title, content)
    res.status(200).json('Added new post')

})

router.put('/:id',async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const {userid, commentId, title, content} = req.body

    const checkid = await checkPostId(id)
    if(checkid === undefined) return res.status(401).json({error: 'Post id did not found. Try another'})

    const checkvalues = await checkPutValues([userid, commentId, title, content])
    if(!checkvalues) return res.status(401).json({error: 'Need value what to change. userid, postid or content'})

    if(userid !== undefined) {
        const value = await checkUserId(userid)
        if(value === undefined) return res.status(401).json({error: 'No user for that id!'})
    }

    if(commentId !== undefined) {
        const value = await checkCommentId(commentId)
        if(value === undefined) return res.status(401).json({error: 'No comment for that id!'})
    }

    await changePostValues(id, userid, commentId, title, content)

    res.status(200).json('value(s) changed')
})

router.delete('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const checkId = await checkPostId(id)
    if(checkId === undefined) return res.status(401).json({error: 'Id did not found'})

    await deletePostById(id)
    res.status(200).json(`Delete post by id: ${id}`)
})


export default router