import express from 'express'
import userRouter from './userRouter'
import commentsRouter from './commentsRouter'
import postsRouter from './postsRouter'

export const server = express()
server.use(express.json())
server.use('/users', userRouter)
server.use('/comments', commentsRouter)
server.use('/posts', postsRouter)

const { PORT } = process.env
server.listen(PORT, () => {
    console.log('Forum API listening to port', PORT)
})
