import {executeQuery} from './db'

export const getAllPosts = async () => {
    const query = 'SELECT "id", "user", title FROM posts'
    const result = await executeQuery(query)
    return result.rows
}

export const getPostById = async (id: number) => {
    console.log(id)
    const query = 'SELECT posts.id AS post_id, posts.user AS post_user, posts.title AS post_title, posts.content AS post_content, posts.post_date AS post_date, (SELECT json_agg(comments) FROM ( SELECT comments.id AS comment_id, comments.user AS comment_user, comments.content AS comment_content, comments.comment_date FROM comments WHERE comments.post_id = posts.id) AS comments) AS comments FROM posts WHERE posts.id = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    return result.rows
}

export const postNewPost = async (user: number, title: string, content: string) => {
    const query = 'INSERT INTO posts ("user", title, "content") VALUES($1, $2, $3)'
    const params = [user, title, content]
    await executeQuery(query, params)
    return    
}

export const changePostValues =async (id: number, userid: number, commentId: number, title: string, content: string) => {
    if(userid !== undefined) {
        const changeQuery = 'UPDATE "posts" SET "user" = $1 WHERE "id" = $2'
        const params = [userid, id]
        await executeQuery(changeQuery, params)
    } 
    
    if(commentId !== undefined) {
        const changeQuery = 'UPDATE "posts" SET comment_id = $1 WHERE "id" = $2'
        const params = [commentId, id]
        await executeQuery(changeQuery, params)
    }

    if(title !== undefined) {
        const changeQuery = 'UPDATE "posts" SET title = $1 WHERE "id" = $2'
        const params = [title, id]
        await executeQuery(changeQuery, params)
    }

    if(content !== undefined) {
        const changeQuery = 'UPDATE "posts" SET content = $1 WHERE "id" = $2'
        const params = [content, id]
        await executeQuery(changeQuery, params)
    }

    return
}

export const deletePostById = async (id: number) => {
    const query = 'DELETE FROM comments WHERE post_id IN (SELECT id FROM posts WHERE "id" = $1)'
    const query2 = 'Delete FROM posts WHERE "id" = $1'
    const params = [id]
    await executeQuery(query, params)
    await executeQuery(query2, params)

    return    
}