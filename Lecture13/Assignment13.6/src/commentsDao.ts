import {executeQuery} from './db'

export const findCommentByUserId = async (id: number) => {
    const query = 'SELECT * FROM "comments" WHERE "user" = $1'
    const params = [id]
    const result = await executeQuery(query, params)

    return result.rows
}

export const postComment = async (id: number, post_id: number, content: string) => {
    post_id = Number(post_id)
    const query = 'INSERT INTO comments ("user", post_id, "content") VALUES($1, $2, $3)'
    const params = [id, post_id, content]
    await executeQuery(query, params)
    return 
    
}

export const deleteComment = async (id: number) => {
    const query = 'DELETE FROM "comments" WHERE "id" = $1'
    const params = [id]
    await executeQuery(query, params)
    return
}

export const changeCommentValues = async (id: number, userid: number, postid: number, content: string) => {
    const query = 'SELECT "user", post_id, "content" FROM "comments"'
    const response = await executeQuery(query)

    if(response.rows[0].user !== userid && userid !== undefined) {
        const changeQuery = 'UPDATE "comments" SET "user" = $1 WHERE "id" = $2'
        const params = [userid, id]
        await executeQuery(changeQuery, params)
    } 
    
    if(response.rows[0].post_id !== postid && postid !== undefined) {
        const changeQuery = 'UPDATE "comments" SET post_id = $1 WHERE "id" = $2'
        const params = [postid, id]
        await executeQuery(changeQuery, params)
    }

    if(response.rows[0].content !== content && content !== undefined) {
        const changeQuery = 'UPDATE "comments" SET content = $1 WHERE "id" = $2'
        const params = [content, id]
        await executeQuery(changeQuery, params)
    }

    return
}