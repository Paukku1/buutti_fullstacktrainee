const getValue = function (): Promise<number> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(Math.random())
        }, Math.random() * 1500)
    })
}

const results = async () => {
    const result1 = await getValue()
    const result2 = await getValue()

    console.log('async await: ', result1, result2)
}


const promiseResults = async () => {
    let a= 0
    getValue()
        .then(val => {
            a = val
            return getValue()
        })
        .then(val => {
            console.log('promise: ', a, val)
        })

    
}

results()

promiseResults()
