"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
/*
interface Response {
    userId: number,
    id: number,
    title: string,
    completed: boolean,
    user?: User
}

interface User {
   id: number,
   name: string,
   username: string,
   email: string,
}
*/
function getData() {
    return __awaiter(this, void 0, void 0, function* () {
        const todo = yield axios_1.default.get('https://jsonplaceholder.typicode.com/todos/');
        const users = yield axios_1.default.get('https://jsonplaceholder.typicode.com/users/');
        const combineData = todo.data.map((todo) => {
            const u = users.data.find((user) => {
                return user.id === todo.userId;
            });
            if (u) {
                const parsedUser = {
                    name: u.name,
                    username: u.username,
                    email: u.email
                };
                todo.user = parsedUser;
                console.log(todo.user);
            }
            return todo;
        });
        console.log(combineData);
    });
}
getData();
