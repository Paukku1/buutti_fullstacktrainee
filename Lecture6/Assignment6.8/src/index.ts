import axios from 'axios'
/*
interface Response {
    userId: number,
    id: number,
    title: string,
    completed: boolean,
    user?: User
}

interface User {
   id: number,
   name: string,
   username: string,
   email: string,
}
*/
async function getData() {
    const todo = await axios.get('https://jsonplaceholder.typicode.com/todos/')
    const users = await axios.get('https://jsonplaceholder.typicode.com/users/')
    
    const combineData = todo.data.map((todo: { userId: number; user: { name: string; username: string; email: string } }) => {
        const u = users.data.find((user: { id: number }) => {
            return user.id === todo.userId
        })       
        
        if(u) {
            const parsedUser = {
                name: u.name,
                username: u.username,
                email: u.email
            }
            todo.user = parsedUser
            
            console.log(todo.user)
        }
        return todo
    })
    
    

    console.log(combineData)
}

getData()