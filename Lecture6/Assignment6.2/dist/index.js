"use strict";
function delay(time, text) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(text);
        }, time);
    });
}
console.log('3');
delay(1000, '2')
    .then(val => {
    console.log(val);
    return delay(1000, '1');
}).then(val => {
    console.log(val);
    return delay(1000, 'GO!');
}).then(val => {
    console.log(val);
});
