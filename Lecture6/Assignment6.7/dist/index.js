"use strict";
const students = [
    { name: 'Markku', score: 99 },
    { name: 'Karoliina', score: 58 },
    { name: 'Susanna', score: 69 },
    { name: 'Benjamin', score: 77 },
    { name: 'Isak', score: 49 },
    { name: 'Liisa', score: 89 },
];
let highestScore = students[0].score;
let lowestScore = students[0].score;
function findHighestAndLowestScores() {
    for (let i = 0; i < students.length; i++) {
        if (highestScore < students[i].score) {
            highestScore = students[i].score;
        }
        else if (lowestScore > students[i].score) {
            lowestScore = students[i].score;
        }
    }
}
function findStudentsScoresMoreAverage() {
    let averageScore = 0;
    for (let i = 0; i < students.length; i++) {
        averageScore = averageScore + students[i].score;
    }
    averageScore = averageScore / students.length;
    console.log('The average is: ', averageScore);
    console.log('Students who scores are more than average');
    for (let i = 0; i < students.length; i++) {
        if (students[i].score > averageScore) {
            console.log(students[i].name);
        }
    }
}
function grades() {
    let newArr = students.concat();
    newArr = students.map(value => {
        if (value.score > 0 && value.score < 40) {
            value = Object.assign(Object.assign(Object.assign({}, value), { grade: 1 }));
        }
        else if (value.score > 39 && value.score < 60) {
            value = Object.assign(Object.assign(Object.assign({}, value), { grade: 2 }));
        }
        else if (value.score > 59 && value.score < 80) {
            value = Object.assign(Object.assign(Object.assign({}, value), { grade: 3 }));
        }
        else if (value.score > 79 && value.score < 95) {
            value = Object.assign(Object.assign(Object.assign({}, value), { grade: 4 }));
        }
        else if (value.score > 94 && value.score < 101) {
            value = Object.assign(Object.assign(Object.assign({}, value), { grade: 5 }));
        }
        else {
            console.log('Error, Something is wrong');
        }
        return value;
    });
    return newArr;
}
findHighestAndLowestScores();
findStudentsScoresMoreAverage();
console.log(grades());
