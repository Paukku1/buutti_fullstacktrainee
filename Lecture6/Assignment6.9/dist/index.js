"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function raceLap() {
    return new Promise((resolve, reject) => {
        const noCrash = Math.random() < 0.97;
        let lapTime = 0;
        if (noCrash) {
            lapTime = Math.floor(Math.random() * 5 + 20);
        }
        return noCrash ? resolve(lapTime) : reject(true);
    });
}
function race(drivers, laps) {
    return __awaiter(this, void 0, void 0, function* () {
        const driversStats = yield Promise.all(drivers.map(driver => {
            const driverStats = { name: driver, stats: { totalTime: 0, bestLap: Infinity, hasCrashed: false } };
            for (let i = 0; i < laps; i++) {
                if (driverStats.stats.hasCrashed === false) {
                    raceLap()
                        .then(result => {
                        if (driverStats.stats.bestLap > result) {
                            driverStats.stats.bestLap = result;
                        }
                        driverStats.stats.totalTime = driverStats.stats.totalTime + result;
                    })
                        .catch(reject => driverStats.stats.hasCrashed = reject);
                }
            }
            return driverStats;
        }))
            .then(result => {
            //console.log(result)
            const noCrashehDrivers = result.filter(driver => driver.stats.hasCrashed === false);
            //console.log(noCrashehDrivers)
            const fastestDriver = noCrashehDrivers.reduce((fastest, driver) => {
                if (driver.stats.totalTime < fastest.stats.totalTime) {
                    fastest = driver;
                    return fastest;
                }
                return fastest;
            });
            return fastestDriver;
        });
        return driversStats;
    });
}
const drivers = ['Henry', 'Hobbit', 'Geralt', 'Ciri', 'Triss'];
race(drivers, 50)
    .then(result => console.log(result))
    .catch(_error => console.log('No winner, everyone crashed!'));
