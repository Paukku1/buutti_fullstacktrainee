interface DriverStats {
name: string
  stats: {
    totalTime: number
    bestLap: number
    hasCrashed: boolean
    }
}

function raceLap (): Promise<number> {
    return new Promise((resolve, reject) => {
        const noCrash = Math.random() < 0.97
        let lapTime = 0
        if(noCrash) {
            lapTime = Math.floor(Math.random() * 5 + 20)
        }
        return noCrash ? resolve(lapTime) : reject(true)
    })
}

async function race (drivers: Array<string>, laps: number): Promise<DriverStats> {
    const driversStats = await Promise.all(drivers.map(driver => {
        const driverStats: DriverStats = { name: driver, stats: { totalTime: 0, bestLap: Infinity, hasCrashed: false } }

        for(let i = 0; i < laps; i++) {
            if(driverStats.stats.hasCrashed === false) {
                raceLap()
                    .then(result => {
                        if(driverStats.stats.bestLap > result) {
                            driverStats.stats.bestLap = result
                        }
                        
                        driverStats.stats.totalTime = driverStats.stats.totalTime + result      
                    })
                    .catch(reject => driverStats.stats.hasCrashed = reject
                    )
            }
            
        }
        return driverStats
    }))
        .then(result => {
            //console.log(result)
            const noCrashehDrivers = result.filter(driver => driver.stats.hasCrashed === false)
            //console.log(noCrashehDrivers)
            const fastestDriver = noCrashehDrivers.reduce((fastest, driver) => {
                if (driver.stats.totalTime < fastest.stats.totalTime) {
                    fastest = driver
                    return fastest
                }
                return fastest
            })
            return fastestDriver
        })

    return driversStats
}
    
const drivers = ['Henry', 'Hobbit', 'Geralt', 'Ciri', 'Triss']
race(drivers, 50)
    .then(result => console.log(result))
    .catch(_error => console.log('No winner, everyone crashed!'))

