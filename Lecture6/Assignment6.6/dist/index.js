"use strict";
const arr = [1, 2, 8, 6, 5, 98, 12, 7];
const arr2 = [-6, 7, -8, -3];
const findMaxNumber = (arr) => {
    let largeNum = arr[0];
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > largeNum) {
            largeNum = arr[i];
        }
    }
    return largeNum;
};
const findSecondMaxNumber = (arr) => {
    let secongLargeNum = arr[0];
    const largeNum = findMaxNumber(arr);
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > secongLargeNum && arr[i] < largeNum) {
            secongLargeNum = arr[i];
        }
    }
    return secongLargeNum;
};
console.log('Largest number in this array is ', findMaxNumber(arr));
console.log('Second largest number in this array is ', findSecondMaxNumber(arr2));
