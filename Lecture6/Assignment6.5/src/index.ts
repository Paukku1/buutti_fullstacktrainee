import axios from 'axios'

interface Response {
    Title: string,
    Year: string
}

async function movieSearch(title: string, year: number) {
    const url = `http://www.omdbapi.com/?apikey=c3a0092f&s=${title}&y=${year}`
    const response = await axios.get(url)
    const typedResult: Response = response.data

    console.log(typedResult)
}
 
movieSearch('jaws', 1975)