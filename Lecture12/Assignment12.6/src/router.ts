import express, { Request, Response } from "express"
import {addProduction} from './dao'

const router  = express.Router()

router.post('/', async (req: Request, res: Response) => {
    const {name, price} = req.body
    await addProduction(name, price)
    res.send('OK')
})



export default router