import {executeQuery} from './db'

export const addProduction = async (name: string, price: number) => {
    const query = ' INSERT INTO products (name, price) VALUES($1, $2)'
    const params = [name, price]
    const result = await executeQuery(query, params)
    console.log(result)
}