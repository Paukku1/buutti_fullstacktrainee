import {executeQuery} from './db'

export const addProduction = async (name: string, price: number) => {
    const query = ' INSERT INTO products (name, price) VALUES($1, $2)'
    const params = [name, price]
    const result = await executeQuery(query, params)
    console.log(result)
}

export const findProducts = async() => {
    const query = 'SELECT * FROM products'
    const result = await executeQuery(query)
    return result.rows
}

export const findProduct = async(id: number) => {
    const query = 'SELECT * FROM products WHERE id = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    return result.rows
}

export const updatingProduct = async (id: number, name: string, price: number) => {

    let query = 'UPDATE products SET name = $1, price = $2 WHERE id = $3 RETURNING *'
    let params = [name, price, id]

    if(name === undefined) {
        query = 'UPDATE products SET price = $1 WHERE id = $2 RETURNING *'
        params = [price, id]
        const result = await executeQuery(query, params)
        return result.rows

    }

    if(price === undefined) {
        query = 'UPDATE products SET name = $1 WHERE id = $2 RETURNING *'
        params = [name, id]
        const result = await executeQuery(query, params)
        return result.rows
    }
    
    const result = await executeQuery(query, params)
    return result.rows
}

export const deleteProduct =async (id:number) => {
    const query = 'DELETE FROM products WHERE id = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    return 
    
}