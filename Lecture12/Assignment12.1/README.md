docker run --name my-postgres --env POSTGRES_PASSWORD=pgpass --env POSTGRES_USER=pguser -p 5432:5432 -d postgres:15.2
docker exec -it my-postgres psql -U pguser
\l
CREATE DATABASE sqlpractice;
\c sqlpractice