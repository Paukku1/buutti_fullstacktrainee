INSERT INTO users (username, full_name, email) 
VALUES ('Lauri', 'Lauri Savela', 'lauri.savela99@gmail.com'),
('Martta65', 'Arto Kaskinen', 'artzaz11@hotmail.com'),
('BeMyWorth', 'Alisa Pelotla', 'alisa.peltola@hotmail.com'),
('Kalevi', 'Kalevi Lammas', 'kalervo@gmail.com'),
('Kaisa', 'Kaisa Angus', 'myownsecret@gmail.com');


INSERT INTO posts ("user", title, "content") 
VALUES (1, 'Koirankakat', 'Voiko ne koirankakat siivota pois tieltä?'),
(2, 'Kesäkukat', 'Käykäähän ostamassa kesäkukkia paikalliselta yrittäjältä. Ovat parhaimillaan!'),
(3, 'Do you scare me?', 'Haluaisiko joku pelata mun kaa LOLia?'),
(4, 'Mansikkamaan ihmeet', 'Katselin tuossa mansikkamaatani ja ensimmäinen mansikka on jo kypsynyt. On tämä ihme!'),
(5, 'Maton pesupaikka auki?', 'Tietääkö kukaan onko matonpesupaikka auki viikonloppuisin?')
;

INSERT INTO comments ("user", post_id, "content") 
VALUES 
(5, 1, 'Todellakin! Koirankakat on hyvä kerätä pois.'),
(4, 1, 'Kyllä ne maatuu aikanaan'),
(1, 3, 'Mikä on LOL? Paljon naurua xD'),
(2, 4, 'Ohhoh, nyt jo on mansikka kypsynyt. Lehdistö paikalle!'),
(3, 5, 'On auki');