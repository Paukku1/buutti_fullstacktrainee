CREATE TABLE "users" (
"id" SERIAL PRIMARY KEY,
 "username" VARCHAR UNIQUE,
 "full_name" varchar NOT NULL,
 "email" varchar NOT NULL
 );
 
 CREATE TABLE "posts" (
	"id" SERIAL PRIMARY KEY,
 	"user" int NOT NULL,
 	"title" varchar NOT NULL,
 	"content" varchar,
	"post_date" timestamp DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY ("user") REFERENCES "users" ("id")
);

CREATE TABLE "comments" (
	"id" SERIAL PRIMARY KEY,
	"user" int NOT NULL,
	"post_id" int NOT NULL,
	"content" varchar NOT NULL,
	"comment_date" timestamp DEFAULT CURRENT_TIMESTAMP,
	 FOREIGN KEY ("user") REFERENCES "users" ("id"),
 	 FOREIGN KEY ("post_id") REFERENCES "posts" ("id")
);