import {executeQuery} from './db'

export const findUsers = async() => {
    const query = 'SELECT id, username FROM "users"'
    const result = await executeQuery(query)
    return result.rows
}

export const findUserById = async(id: number) => {
    const query = 'SELECT * FROM "users" WHERE id = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log(id)
    console.log(result.rows)
    return result.rows[0]
}


export const addUser = async (username: string, fullname: string, email: string) => {
    const query = ' INSERT INTO users (username, full_name, email) VALUES($1, $2, $3)'
    const params = [username, fullname, email]
    await executeQuery(query, params)
    return 
}
