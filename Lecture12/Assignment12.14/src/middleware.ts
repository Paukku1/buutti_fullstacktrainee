import express, { Request, Response } from "express"
import {executeQuery} from './db'



// check POST from userrouter, postsrouter and commentsrouter
export function checkUserPostValues (username: string, fullname: string, email: string) {
    let allValues = true
    if(username === undefined || username === '') return allValues = false 
    if(fullname === undefined|| fullname === '') return allValues = false 
    if(email === undefined || email === '') return allValues = false
    
    return allValues
}

export async function checkPostPostValues (user: number, title: string, content: string) {
    let allValues = true

    const query = 'SELECT * FROM users'
    const result = await executeQuery(query)
    const findUser = result.rows.find(dbuser => dbuser.id === user)
    
    if(findUser === undefined) return allValues = false 
    if(title === undefined || title === '') return allValues = false 
    if(content === undefined || content === '') return allValues = false
    
    return allValues
}

export async function checkPostCommentValues (id: number, post_id: number, content: string) {
    let allValues = true

    const query = 'SELECT * FROM "users"'
    const result = await executeQuery(query)
    const findUser = result.rows.find(user => user.id === id)
    
    const querypost = 'SELECT * FROM posts'
    const resultpost = await executeQuery(querypost)
    const findPostId = resultpost.rows.find(post => post.id === post_id)

    if(findUser === undefined) return allValues = false 
    if(findPostId === undefined) return allValues = false 
    if(content === undefined || content === '') return allValues = false
    
    return allValues
}


// Check username, user
export async function checkValidUsername (username: string) {
    const query = 'SELECT * FROM "users"'
    const result = await executeQuery(query)
    const findusername = result.rows.find(user => user.username === username)

    return findusername
}

export async function checkCommentUser (id: number) {
    const query = 'SELECT * FROM "comments"'
    const result = await executeQuery(query)
    const findUser = result.rows.find(user => user.id === id)
        
    return findUser
}

export async function checkIdFunction (id: number) {
    const query = 'SELECT * FROM posts'
    const result = await executeQuery(query)
    const findId = result.rows.find(post => post.id === id)
    return findId
}

