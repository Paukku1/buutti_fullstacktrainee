import {executeQuery} from './db'

export const getAllPosts = async () => {
    const query = 'SELECT "id", "user", title FROM posts'
    const result = await executeQuery(query)
    return result.rows
}

export const getPostById = async (id: number) => {
    console.log(id)
    const query = 'SELECT posts.id AS post_id, posts.user AS post_user, posts.title AS post_title, posts.content AS post_content, posts.post_date AS post_date, (SELECT json_agg(comments) FROM ( SELECT comments.id AS comment_id, comments.user AS comment_user, comments.content AS comment_content, comments.comment_date FROM comments WHERE comments.post_id = posts.id) AS comments) AS comments FROM posts WHERE posts.id = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    return result.rows
}

export const postNewPost = async (user: number, title: string, content: string) => {
    const query = 'INSERT INTO posts ("user", title, "content") VALUES($1, $2, $3)'
    const params = [user, title, content]
    await executeQuery(query, params)
    return    
}