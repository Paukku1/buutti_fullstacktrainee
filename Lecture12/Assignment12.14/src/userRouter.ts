import express, { Request, Response } from "express"
import {addUser, findUsers, findUserById} from './userDao'
import {checkUserPostValues, checkValidUsername} from "./middleware"

const router  = express.Router()

router.get('/', async (req: Request, res: Response) => {
    const result = await findUsers()
    res.send(result)
})

router.get('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await findUserById(id)
    res.send(result)
})

router.post('/', async (req: Request, res: Response) => {
    const {username, fullname, email} = req.body
    const result = checkUserPostValues(username, fullname, email)
    if(!result) return res.status(404).send('Missing params. Added username, fullname and email?')

    const checkusername = await checkValidUsername(username)
    if(checkusername) return res.status(401).send({error: 'Username is already exist'})

    await addUser(username, fullname, email)
    res.send('Added new user')
})

export default router