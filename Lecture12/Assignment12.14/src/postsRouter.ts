import express, {Request, Response} from 'express'
import { getAllPosts, getPostById, postNewPost } from './postsDao'
import { checkIdFunction, checkPostPostValues } from './middleware'

const router = express.Router()

router.get('/', async (req:Request, res: Response) => {
    const result = await getAllPosts()

    res.status(200).send(result)
})

router.get('/:id',async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const checkId = await checkIdFunction(id)
    if(checkId === undefined) return res.status(404).send({error: 'Id did not found'})

    const result = await getPostById(id)
    res.status(200).send(result)
})

router.post('/',async (req: Request, res: Response) => {
    const {user, title, content} = req.body
    const checkValues = await checkPostPostValues(user, title, content)

    if(!checkValues) return res.status(404).send({error: 'Missing value: Values that needed: user, title and content. Or wrong user'})

    await postNewPost(user, title, content)
    res.status(200).send('Added new post')

})

export default router