import express, { Request, Response } from "express"
import {findCommentByUserId, postComment} from './commentsDao'
import {checkPostCommentValues, checkCommentUser} from './middleware'

const router = express.Router()

router.get('/:userid', async (req:Request, res: Response) => {
    const id = Number(req.params.userid)

    const checkUser = await checkCommentUser(id)
    if(checkUser === undefined) return res.status(404).send({error: "This user have not any comments"})
    
    const result = await findCommentByUserId(id)
    res.send(result)    
})

router.post('/',async (req:Request, res:Response) => {
    const {user, post_id, content} = req.body    
    const result = await checkPostCommentValues(user, post_id, content)

    if(!result) return res.status(404).send({error: 'Params missing or wrong. Params: user, post_id, content'})

    postComment(user, post_id, content)

    return res.status(200).send('Added new comment')
})

export default router