import {executeQuery} from './db'

export const findUsers = async() => {
    const query = 'SELECT id, username FROM "users"'
    const result = await executeQuery(query)
    return result.rows
}

export const findUserById = async(id: number) => {
    const query = 'SELECT * FROM "users" WHERE id = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log(id)
    console.log(result.rows)
    return result.rows[0]
}


export const addUser = async (username: string, fullname: string, email: string) => {
    const query = ' INSERT INTO users (username, full_name, email) VALUES($1, $2, $3)'
    const params = [username, fullname, email]
    await executeQuery(query, params)
    return 
}

export const deleteUser =async (id: number) => {
    const query = 'DELETE FROM comments WHERE post_id IN (SELECT id FROM posts WHERE "user" = $1)'
    const query2 = 'DELETE FROM "comments" WHERE "user" = $1'
    const query3 = 'DELETE FROM posts WHERE "user" = $1'
    const query4 = 'DELETE FROM "users" WHERE "username" = $1'
    const params = [id]
    console.log(id)
    await executeQuery(query, params)
    await executeQuery(query2, params)
    await executeQuery(query3, params)
    await executeQuery(query4, params)
    return    
}