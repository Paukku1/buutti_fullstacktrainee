import {executeQuery} from './db'

export const findCommentByUserId = async (id: number) => {
    const query = 'SELECT * FROM "comments" WHERE "user" = $1'
    const params = [id]
    const result = await executeQuery(query, params)

    return result.rows
}

export const postComment = async (id: number, post_id: number, content: string) => {
    post_id = Number(post_id)
    const query = 'INSERT INTO comments ("user", post_id, "content") VALUES($1, $2, $3)'
    const params = [id, post_id, content]
    await executeQuery(query, params)
    return 
    
}

export const deleteComment = async (id: number) => {
    const query = 'DELETE FROM "comments" WHERE "id" = $1'
    const params = [id]
    await executeQuery(query, params)
    return
}