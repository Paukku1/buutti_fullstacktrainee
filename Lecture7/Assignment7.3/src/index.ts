export

function calculator (operator: string, num1: number, num2: number) {
    if(operator === '+') {
        return num1 + num2
    }
    else if(operator === '-') {
        return num1 - num2
    }
    else if(operator === '*') {
        return num1 * num2
    }
    else if(operator === '/') {
        if(num1 === 0 || num2 === 0){
            return 'Can not do that!'
        }
        return num1 / num2
    }
    else {
        return 'Can not do that!'
    }
    
}

console.log(calculator('+', 1, 2))