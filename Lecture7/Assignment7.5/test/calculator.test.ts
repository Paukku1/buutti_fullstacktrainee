import {calculator} from '../src/index'

test('1 + 2 = 3', () => {
    expect(calculator('+', 1, 2)).toBe(3)
})
test('2 - 1 = 1', () => {
    expect(calculator('-', 2, 1)).toBe(1)
})
test('1 * 2 = 3', () => {
    expect(calculator('*', 1, 2)).toBe(2)
})
test('errori', () => {
    expect(calculator('%', 1, 2)).toBe('Can not do that!')
})

test('0 / 2 = ?', () => {
    expect(calculator('/', 0, 2)).toBe('Can not do that!')
})

test('4 / 2 = ?', () => {
    expect(calculator('/', 4, 2)).toBe(2)
})