import {converter} from '../src/index'

test('convert dl to different units', () => {
    expect(converter(60, 'dl', 'l')).toBe(6)
    expect(converter(60, 'dl', 'oz')).toBe(202)
    expect(converter(60, 'dl', 'cup')).toBe(25)
    expect(converter(60, 'dl', 'pt')).toBe(12)
})

test('convert l to different units', () => {
    expect(converter(60, 'l', 'dl')).toBe(600)
    expect(converter(60, 'l', 'oz')).toBe(20)
    expect(converter(60, 'l', 'cup')).toBe(2)
    expect(converter(60, 'l', 'pt')).toBe(1)
})

test('convert oz to different units', () => {
    expect(converter(60, 'oz', 'dl')).toBe(17)
    expect(converter(60, 'oz', 'l')).toBe(1)
    expect(converter(60, 'oz', 'cup')).toBe(7)
    expect(converter(60, 'oz', 'pt')).toBe(3)
})

test('convert cup to different units', () => {
    expect(converter(60, 'cup', 'dl')).toBe(141)
    expect(converter(60, 'cup', 'l')).toBe(14)
    expect(converter(60, 'cup', 'oz')).toBe(480)
    expect(converter(60, 'cup', 'pt')).toBe(30)
})

test('convert pt to different units', () => {
    expect(converter(60, 'pt', 'dl')).toBe(283)
    expect(converter(60, 'pt', 'l')).toBe(28)
    expect(converter(60, 'pt', 'oz')).toBe(960)
    expect(converter(60, 'pt', 'cup')).toBe(120)
})
