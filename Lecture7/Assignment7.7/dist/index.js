"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.converter = void 0;
const amount = parseInt(process.argv[2]);
const convert = process.argv[3];
const convertTo = process.argv[4];
function converter(amount, convert, convertTo) {
    if (convert === 'dl') {
        return convertDeciliter(amount, convertTo);
    }
    else if (convert === 'l') {
        return convertLiter(amount, convertTo);
    }
    else if (convert === 'oz') {
        return convertOunce(amount, convertTo);
    }
    else if (convert === 'cup') {
        return convertCup(amount, convertTo);
    }
    else if (convert === 'pt') {
        return converPint(amount, convertTo);
    }
}
exports.converter = converter;
function convertDeciliter(amount, convertTo) {
    if (convertTo === 'l') {
        return (amount / 10);
    }
    else if (convertTo === 'oz') {
        return Math.floor(amount * 3.38);
    }
    else if (convertTo === 'cup') {
        return Math.floor(amount * 0.4226);
    }
    else if (convertTo === 'pt') {
        return Math.floor(amount * 0.211);
    }
    else {
        return 'Error. Wrong unit';
    }
}
function convertLiter(amount, convertTo) {
    if (convertTo === 'dl') {
        return Math.floor(amount * 10);
    }
    else if (convertTo === 'oz') {
        return Math.floor(amount * 0.338);
    }
    else if (convertTo === 'cup') {
        return Math.floor(amount * 0.04226);
    }
    else if (convertTo === 'pt') {
        return Math.floor(amount * 0.0211);
    }
    else {
        return 'Error. Wrong unit';
    }
}
function convertOunce(amount, convertTo) {
    if (convertTo === 'dl') {
        return Math.floor(amount * 0.295);
    }
    else if (convertTo === 'l') {
        return Math.floor(amount * 0.0295);
    }
    else if (convertTo === 'cup') {
        return Math.floor(amount * 0.125);
    }
    else if (convertTo === 'pt') {
        return Math.floor(amount * 0.0625);
    }
    else {
        return 'Error. Wrong unit';
    }
}
function convertCup(amount, convertTo) {
    if (convertTo === 'dl') {
        return Math.floor(amount * 2.365);
    }
    else if (convertTo === 'l') {
        return Math.floor(amount * 0.2365);
    }
    else if (convertTo === 'oz') {
        return Math.floor(amount * 8);
    }
    else if (convertTo === 'pt') {
        return Math.floor(amount * 0.5);
    }
    else {
        return 'Error. Wrong unit';
    }
}
function converPint(amount, convertTo) {
    if (convertTo === 'dl') {
        return Math.floor(amount * 4.73);
    }
    else if (convertTo === 'l') {
        return Math.floor(amount * 0.473);
    }
    else if (convertTo === 'oz') {
        return Math.floor(amount * 16);
    }
    else if (convertTo === 'cup') {
        return Math.floor(amount * 2);
    }
    else {
        return 'Error. Wrong unit';
    }
}
console.log(converter(amount, convert, convertTo));
