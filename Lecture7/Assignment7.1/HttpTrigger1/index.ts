import { AzureFunction, Context, HttpRequest } from "@azure/functions"

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    const input = req.body.input.toUpperCase()
    
    context.res = {
        status: 200, /* Defaults to 200 */
        body: input
    };

    

};

export default httpTrigger;