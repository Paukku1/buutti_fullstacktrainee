import { AzureFunction, Context, HttpRequest } from "@azure/functions"
import axios from "axios"

interface ResponseData {
    message: string,
    image_url: string  
}

const randomDogUrl = "https://dog.ceo/api/breeds/image/random"
const randomCatUrl = "https://cataas.com/cat"
const randomFoxUrl = "https://randomfox.ca/floof/"
let requestCount = 0

const getRandomDogImage = async (): Promise<string> => {
    const response = await axios.get(randomDogUrl);
    return response.data.message;
  }
  
  const getRandomCatImage = async (): Promise<string> => {
    const response = await axios.get(randomCatUrl);
    return response.request.res.responseUrl;
  }
  
  const getRandomFoxImage = async (): Promise<string> => {
    const response = await axios.get(randomFoxUrl);
    return response.data.image;
  }

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    
    let message: string;
    let imageUrl: string;
    requestCount++

    if (requestCount % 13 === 0) {
        imageUrl = await getRandomFoxImage();
        message = "It's a fox!";
    } else {
        const animal = Math.random() < 0.5 ? "cat" : "dog";
        if (animal === "cat") {
        imageUrl = await getRandomCatImage();
        message = "It's a cat!";
        } else {
        imageUrl = await getRandomDogImage();
        message = "It's a dog!";
        }
    }

  const responseData: ResponseData = {
    message,
    image_url: imageUrl,
  };
    const htmlResponse = `
        <!DOCTYPE html>
        <html>
            <head>
            <title>Random Animal Picture</title>
            </head>
            <body>
            <h1>${message}</h1>
            <img src="${imageUrl}" alt="Random Animal Picture">
            </body>
        </html>
        `

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: htmlResponse
    };

};

export default httpTrigger;