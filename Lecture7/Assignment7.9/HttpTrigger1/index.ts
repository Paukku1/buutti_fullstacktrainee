import { AzureFunction, Context, HttpRequest } from "@azure/functions"

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    let responseMessage = ''

    if(req.body.min && req.body.max && req.body.bool) {
        const min = req.body.min
        const max = req.body.max
        const bool = req.body.bool
        
        if(bool === true) {
            const randomNumber = Math.floor(Math.random() * (max - min) + min)
            responseMessage = `Random number is: ${randomNumber}`
        } else {
            const randomNumber = Math.random() * (max - min) + min
            responseMessage = `Random number is: ${randomNumber}`
        }
    }
    else {
        responseMessage = 'Sorry we need a min, a max and a boolean value to return a random number'
    }
    

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: responseMessage
    };

};

export default httpTrigger;