"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../src/index");
test('1 + 2 = 3', () => {
    expect((0, index_1.calculator)('+', 1, 2)).toBe(3);
});
test('2 - 1 = 1', () => {
    expect((0, index_1.calculator)('-', 2, 1)).toBe(1);
});
test('1 * 2 = 3', () => {
    expect((0, index_1.calculator)('*', 1, 2)).toBe(2);
});
test('errori', () => {
    expect((0, index_1.calculator)('%', 1, 2)).toBe('Can not do that!');
});
test('0 / 2 = ?', () => {
    expect((0, index_1.calculator)('/', 0, 2)).toBe('Can not do that!');
});
describe('Calculator', () => {
    it('1 + 2 = 3', () => {
        const result = (0, index_1.calculator)('+', 1, 2);
        expect(result).toBe(3);
    });
    it('3 - 1 = 2', () => {
        const result = (0, index_1.calculator)('-', 3, 1);
        expect(result).toBe(2);
    });
    it('3 * 1 = 3', () => {
        const result = (0, index_1.calculator)('*', 3, 1);
        expect(result).toBe(3);
    });
});
