import {calculator} from '../src/index'

describe('Calculator', () => {
    it('1 + 2 = 3', () => {
        const result = calculator('+', 1,2)
        expect(result).toBe(3)
    })
   
    it('3 - 1 = 2', () => {
        const result = calculator('-',3,1)
        expect(result).toBe(2)
    })

    it('3 * 1 = 3', () => {
        const result = calculator('*',3,1)
        expect(result).toBe(3)
    })

    it('3 / 1 = 3', () => {
        const result = calculator('/',3,1)
        expect(result).toBe(3)
    })

    it('3 / 0 = ?', () => {
        const result = calculator('/',3,0)
        expect(result).toBe('Can not do that!')
    })

    it('3 % 1 = ?', () => {
        const result = calculator('%',3,1)
        expect(result).toBe('Can not do that!')
    })
 })
 