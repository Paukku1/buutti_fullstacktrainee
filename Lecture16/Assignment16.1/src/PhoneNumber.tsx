import { useState, ChangeEvent  } from 'react'

interface Props {
    addPhoneNumber: (newNumber: string ) => void
}

function PhoneNumber(props: Props) {
  const [newPhoneNumber, setNewPhoneNumbers] = useState('')

  const changeValue = (event: ChangeEvent<HTMLInputElement>) => {
    const number = event.target.value
    
    if(number.length === 10) {
        console.log(number)
        setNewPhoneNumbers(number)
        console.log(newPhoneNumber)
        props.addPhoneNumber(newPhoneNumber)
        setNewPhoneNumbers('')
    } else {
        setNewPhoneNumbers(number)
    }
  }

  return (
    
    <>
        <h1>Assignment16.1</h1>

        <input type="number" 
        value={newPhoneNumber} 
        onChange={changeValue} />
      
    </>
  )
}

export default PhoneNumber
