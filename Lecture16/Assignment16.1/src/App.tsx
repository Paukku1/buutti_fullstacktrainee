import PhoneNumber from "./PhoneNumber"
import { useState } from "react"


function App() {
  const [phoneNumber, setPhoneNumbers] = useState<Array<string>>([]) 

  const addPhoneNumber = (newNumber: string) => {
    setPhoneNumbers([...phoneNumber, newNumber])
  }

  return (
    <>
      <PhoneNumber addPhoneNumber={addPhoneNumber}  />

      {phoneNumber.map((number, i) => {
            return <li key={'phonenumber' + i}><i>{number}</i></li>
        })}   
      
    </>
  )
}

export default App
