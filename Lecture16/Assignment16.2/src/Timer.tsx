import { useState, useEffect } from 'react'


function Timer() {
  const [time, setTime] = useState(0)
  const [minutes, setMinutes] = useState<number>()

  useEffect(() => {
    const timeout = setTimeout(() => setTime(time + 1), 1000)

    if(minutes !== undefined && time >= 60) {
        setMinutes(minutes + 1)
        setTime(0)
    }
    return () => clearTimeout(timeout)
  }, [time, minutes])

  const onClick = () => {
    setTime(0)
    setMinutes(undefined)
  }

  const onClickMinute = () => {
    setMinutes(0)
  }
  let min = ''
  if(minutes !== undefined) {
    min = `${minutes} : ` 
  }

  return (
    <>
      <h1>Assignment16.2</h1>
      
      {min}
      {time}
      
      <br/>
      
      <button onClick={onClick}> Reset</button>
      <button onClick={onClickMinute}> Show minutes</button>
    </>
  )
}

export default Timer
