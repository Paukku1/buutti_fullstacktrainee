import { useState, } from "react"
import EditContact from "./EditContact"

interface Contact {
  id: number
  name: string
  phone: string
  email: string
  address: string
  notes: string
}

interface ContactDetailProps {
  contacts: Contact[]
  id: number
  istrue: boolean
  removeContact: (id: number) => void
  clickAddNewC: () => void
  addContact: (newContact: Contact) => void
}

function ContactDetail(props: ContactDetailProps) {
    const [isEditing, setIsEditing] = useState(false);

    const handleEditClick = () => {
      setIsEditing(true);
    }

  const removeC = (id:number) => {
    props.removeContact(id)
    }

    if (props.istrue) {
      const contact = props.contacts.find((contact) => contact.id === props.id);

      if (contact) {
        return (
          <>
            {isEditing ? (
              <EditContact contact={contact} addContact={props.addContact} clickAddNewC={props.clickAddNewC} setIsEditing={setIsEditing} />
              ) :(
                <div>
                  <h1>{contact.name}</h1>
                  <p><i>Phone</i>: {contact.phone}</p>
                  <p><i>Email</i>: {contact.email}</p>
                  <p><i>Address</i>: {contact.address}</p>
                  <p><i>Notes</i>: {contact.notes}</p>

                  <button onClick={() => removeC(contact.id)}>Remove</button>
                  <button onClick={() => handleEditClick()}>Edit</button>
                  </div>
              )
      
            }
          </>
      )
    }
  }
  else {
    return (
      <h1>Contact Manager</h1>
    )
  }
}

export default ContactDetail