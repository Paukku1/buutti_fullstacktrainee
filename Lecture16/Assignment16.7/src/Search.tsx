import { useState, useEffect } from 'react'
interface Contact {
    id: number
    name: string
    phone: string
    email: string
    address: string
    notes: string
  }
  

interface SearchProps {
    contacts: Contact[]
    searchContacts: (foundedContacts: string) => void
}
function Search (props:SearchProps) {
    const [searchName, setSearchName] = useState('')
    //KÄYTÄ USEEFFECTIÄ JA SIT TOISESSA PAIKKAA FILTTERÖINTIÄ

    useEffect(() => {
        props.searchContacts(searchName)
        }, [searchName])
    
    return (
        <>
        <input value={searchName} placeholder="Search" onChange={(e) => setSearchName(e.target.value)} />
        </>
    )
}

export default Search