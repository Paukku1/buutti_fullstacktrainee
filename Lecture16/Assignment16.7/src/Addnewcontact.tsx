import { useState, ChangeEvent } from "react"
interface Contact {
    id: number,
    name: string,
    phone: string
    email: string,
    address: string,
    notes: string
}

interface Props {
    addContact: (newContact: Contact) => void
    clickAddNewC: () => void
}

function Addnewcontact (props:Props) {
    const [newContact, setNewContact] = useState<Contact> ({
        id: 0,
        name: "",
        phone: "",
        email: "",
        address: "",
        notes: ""
    })

    const handleChange = (event:ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value
        setNewContact({...newContact, [event.target.name]: value})
    }

    const checkValues = () => {
        if(newContact.name === ''){
            console.log('error: Name is empty')
        } else if(newContact.email === ''){
            console.log('error: Email is empty')
        } else if(newContact.phone === ''){
            console.log('error: Phone is empty')
        } else if(newContact.address === ''){
            console.log('error: Address is empty')
        } else {
            props.addContact(newContact)
        }
    }

    return (
        <div>
            <h1>Add new contact</h1>
            <div> Name <br />
                <input name="name"
                value={newContact.name}
                onChange={handleChange} />
            </div>
            <div> Phone <br />
                <input name="phone"
                value={newContact.phone}
                onChange={handleChange} />
            </div>
            <div> Email <br />
                <input name="email"
                value={newContact.email}
                onChange={handleChange}/>
            </div>
            <div> Address <br />
                <input name="address"
                value={newContact.address}
                onChange={handleChange}/>
            </div>
            <div> Notes <br />
                <input name="notes"
                value={newContact.notes}
                onChange={handleChange}/>
            </div>

            <button onClick={checkValues}>Save</button>
            <button onClick={props.clickAddNewC}>Cancel</button>
            
        </div>
    )
}

export default Addnewcontact