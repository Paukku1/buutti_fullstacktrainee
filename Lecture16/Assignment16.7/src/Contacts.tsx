import { useState } from "react"
import Addnewcontact from "./Addnewcontact"
import "./Contacts.css"
import ContactDetail from "./ContactDetail"
import Search from "./Search"

interface Contact {
    id: number,
    name: string,
    phone: string
    email: string,
    address: string,
    notes: string
}

function Contacts() {
    const [contacts, setContacts] = useState<Contact[]> ([])
    const [findContacts, setFindContacts] =  useState<Contact[]> ([])
    
    const [addNewC, setAddNewC] = useState(false)
    const [infoC, setinfoC] = useState(false)
    const [infoForId, setinfoForId] = useState(Number)


    const clickAddNewC = () => {
        if(!addNewC) {
            setAddNewC(true)
            setinfoC(false)
        } else {
            setAddNewC(false)
        }
    }

    const addContact = (newContact: Contact) => {

        const existingContactIndex = contacts.findIndex((contact) => contact.id === newContact.id)
      
        if (existingContactIndex !== -1) {
            setContacts((prevContacts) => {
                const updatedContacts = [...prevContacts];
                updatedContacts[existingContactIndex] = newContact;
                return updatedContacts;
            })
        } else {
            const timestamp = Date.now();
            const newId = timestamp.toString();

            const newContactWithId = {
            ...newContact,
            id: Number(newId)
            }
            
            setContacts((prevContacts) => [...prevContacts, newContactWithId])
            setFindContacts((prevContacts) => [...prevContacts, newContactWithId])
            setAddNewC(false)
        }
    }

    const searchContacts = (searchString: string) => {
        const foundedContacts = contacts.filter(contact => {
            if(searchString === '') {
                return contact
            } else {
                return contact.name.toLowerCase().includes(searchString)
            }
        })

        setFindContacts(foundedContacts)
    }

    const detailInfo = (id: number) => {
        if(addNewC) setAddNewC(false)
        
        if(infoC) setinfoC(false)
        
        else {
            setinfoC(true)
            setinfoForId(id)
        }
    }

    const removeContact = (id: number) => {
        setContacts(current => current.filter(c => c.id !== id))
        setFindContacts(current => current.filter(c => c.id !== id))
    }
        
    return (
      <>
      <button onClick={clickAddNewC}>Add Contact</button>
      <Search contacts={contacts} searchContacts={searchContacts} />

      <div>
            <div className="contacts">
                {findContacts.map((contact) => {
                    return <div key={contact.id} onClick={() => detailInfo(contact.id)}>{contact.name}</div>
                })}

            </div>

            <div className="contacts">
                {addNewC 
                ? 
                <Addnewcontact addContact={addContact} clickAddNewC={clickAddNewC} />
                 
                : <ContactDetail id={infoForId} istrue={infoC} contacts={contacts} removeContact={removeContact} clickAddNewC={clickAddNewC} addContact={addContact} /> }
                
                

            </div>
        </div>
      </>
    )
  }
  
  export default Contacts