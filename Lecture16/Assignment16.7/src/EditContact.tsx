import { useState, ChangeEvent } from "react"

interface Contact {
    id: number
    name: string
    phone: string
    email: string
    address: string
    notes: string
  }
  

interface EditContactProps {
    contact: Contact;
    addContact: (newContact: Contact) => void
    clickAddNewC: () => void
    setIsEditing:  (value: boolean) => void
  }


function EditContact (props: EditContactProps) {
    const [editContact, setEditContact] = useState<Contact> ({
        id: props.contact.id,
        name: props.contact.name,
        phone: props.contact.phone,
        email: props.contact.email,
        address: props.contact.address,
        notes: props.contact.notes
    })

    const handleChange = (event:ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value
        setEditContact({...editContact, [event.target.name]: value})
    }

    const checkValues = () => {
        if(editContact.name === ''){
            console.log('error: Name is empty')
        } else if(editContact.email === ''){
            console.log('error: Email is empty')
        } else if(editContact.phone === ''){
            console.log('error: Phone is empty')
        } else if(editContact.address === ''){
            console.log('error: Address is empty')
        } else {
            props.addContact(editContact)
            props.setIsEditing(false)
        }
    }
    
    return (
        <div>
            <h1>Edit contact</h1>
            <div> Name <br />
                <input name="name"
                value={editContact.name}
                onChange={handleChange} 
                placeholder={editContact.name} />
            </div>
            <div> Phone <br />
                <input name="phone"
                value={editContact.phone}
                onChange={handleChange} 
                placeholder={editContact.phone} />
            </div>
            <div> Email <br />
                <input name="email"
                value={editContact.email}
                onChange={handleChange}
                placeholder={editContact.email} />
            </div>
            <div> Address <br />
                <input name="address"
                value={editContact.address}
                onChange={handleChange}
                placeholder={editContact.address} />
            </div>
            <div> Notes <br />
                <input name="notes"
                value={editContact.notes}
                onChange={handleChange}
                placeholder={editContact.notes} />
            </div>

            <button onClick={checkValues}>Save</button>
            <button onClick={props.clickAddNewC}>Cancel</button>
            
        </div>
    )
}
export default EditContact