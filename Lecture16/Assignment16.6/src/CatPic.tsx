import { useState, useEffect } from "react"
import axios from "axios"


function CatPic () {
    const header = <h1>Assignment 16.6 - More Cats</h1>

    const [catpic, setCatpic] = useState('')
    const [text, setNewText] = useState('')

    async function getCatPic() {
        const url = 'https://cataas.com/cat?html=true&json=true'

        await axios.get(url)
            .then(response => {
                setCatpic('https://cataas.com/' + response.data.url)
            })
            .catch(error => {
                console.log('Error fetching cat image:', error)
            })
    }

    async function getCatPicWithText() {
        const url = `https://cataas.com/cat/says/${text}?html=true&json=true` 

        await axios.get(url)
            .then(response => {
                setCatpic('https://cataas.com/' + response.data.url)
            })
            .catch(error => {
                console.log('Error fetching cat image:', error)
            })
    }

  useEffect(() => {
    getCatPic()
    }, [])

    const reloadImage = () => {
        
        if(text !== '') {
            console.log(text)
            getCatPicWithText()
        }
        getCatPic()
    }


    return (
        <>
            {header}
            <input type="text" value={text} onChange={e => setNewText(e.target.value)}/><br />
            <button onClick={reloadImage}>Reload</button><br />
            <img src={catpic} height='400px' />
            
        </>
    )
}
    


export default CatPic