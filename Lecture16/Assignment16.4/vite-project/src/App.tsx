import { useState } from 'react'
import './App.css'

function App() {
  const [bingoNumbers, setBingoNumbers] = useState<Array<number>>([])

  const addBingoNumber = () => {
    const randomNumber = Math.floor(Math.random() * 75 + 1)
    if (bingoNumbers.length < 75) {
      if (bingoNumbers.find((n) => n === randomNumber)) {
      console.log("same number")
      addBingoNumber()
    } else {
      setBingoNumbers([...bingoNumbers, randomNumber])
    }
    }
  }

  const resetBingoNumbers = () => {
    setBingoNumbers([])
  }

  return (
    <>
      <h1>Bingo</h1>
      <div className="bingoGame">
        <button onClick={() => addBingoNumber()}>
          +
        </button>
        <button onClick={() => resetBingoNumbers()}>
          reset
        </button>
        <div className="bingoBalls">{bingoNumbers.map((number, i) => {
          return <div className="bingoBall" key={"bingoNumber" + i}>{number}</div>
        })}</div>
      </div>
    </>
  )
}

export default App
