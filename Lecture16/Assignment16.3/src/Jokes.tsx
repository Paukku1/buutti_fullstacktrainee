import axios from 'axios'
import { useState, useEffect } from 'react'

function Jokes() {
  const [joke, setJoke] = useState()

  useEffect(() => {
    async function joke() {
        const url = 'https://api.api-ninjas.com/v1/dadjokes?limit=1'
        const config = {
            headers: {
                'X-Api-Key': 'qFRwfcoEjiLRvRYhJirWtQ==1JTjQy3ah2MoAPgM'
            }
        }
        const response = await axios.get(url, config)
        const data = await response.data[0].joke

        setJoke(data)

    }
    joke()
  }, [])

  
  return (
    <>
      <h1>Assignment16.3</h1>
      {joke}
      
    </>
  )
}

export default Jokes
