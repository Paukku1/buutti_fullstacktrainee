"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server = (0, express_1.default)();
const data = {
    counter: 0
};
server.get('/:counter', (req, res) => {
    data.counter++;
    res.send(data.counter.toString());
});
server.listen(3000);
