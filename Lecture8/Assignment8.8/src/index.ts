import express, { Request, Response, NextFunction } from 'express'
import { middleware, unknownEndpoint } from './middlewares'


const server = express()
server.use(express.json())
server.use(middleware)

interface Student {
  id: number,
  name: string,
  email: string
}
let students: Student[] = []

server.get('/students', (req: Request, res: Response) => {
  const newArr = students.map(student => student.id)
  res.send(newArr)
})

server.get('/student/:id', (req: Request,  res: Response, next) => {
  const student = students.find(student => student.id.toString() === req.params.id)

  if(student) {
    res.status(200).send(student)
  }
  next()
})

server.post('/student', (req: Request, res: Response) => {
  if(!req.body.id || !req.body.name || !req.body.email) {
    res.status(400).send({error: "Some of parameters is missing"})
  } else {
    const newStudent: Student = {
      id : req.body.id,
      name: req.body.name,
      email: req.body.email
    }
    students.push(newStudent)
    res.status(200).send(students) 
  }
})

server.use(unknownEndpoint)

server.listen(3000)
