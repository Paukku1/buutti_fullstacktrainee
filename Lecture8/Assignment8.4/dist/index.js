"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server = (0, express_1.default)();
const data = [
    {
        name: 'Jane',
        counter: 0
    },
    {
        name: 'Tarzan',
        counter: 0
    }
];
server.get('/counter/:name', (req, res) => {
    const name = req.params.name;
    data.find(item => {
        if (item.name === name) {
            item.counter++;
            res.send(item.name + item.counter.toString());
        }
    });
});
server.listen(3000);
