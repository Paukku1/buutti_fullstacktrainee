import express, { Request, Response, NextFunction } from 'express'

interface ObjectWithRequiredFields {
  name: string,
  score: number
}

  export const validator = ( req: Request, res: Response, next: NextFunction) => {
    const inputArray: ObjectWithRequiredFields[] = req.body

    if (!Array.isArray(inputArray)) {
      res.status(400).send({ error: 'Input should be an array' })
      return
    }

    for (const obj of inputArray) {
      if (typeof obj !== 'object' || Array.isArray(obj) || obj === null) {
        res.status(400).send({ error: 'Array elements should be objects' })
        return
      } else {
        const requiredFields: string[] = ['name', 'score'];
    
        for (const field of requiredFields) {
          if (!(field in obj)) {
            res.status(400).send({ error: `Missing required field: ${field}` });
            return;
          }
        }}
    }
    next()
}