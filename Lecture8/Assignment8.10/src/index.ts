import express, { Request, Response, NextFunction } from 'express'
import { validator } from './middlewares'


const server = express()
server.use(express.json())

interface Student {
  name: string,
  score: number
}

server.use(validator)

server.get('/students', (req: Request, res: Response) => {
  const array = req.body
  let average = 0
  let HighestScore = 0
  let studentHighestScore = ''
  let percentage = 0

  array.forEach((element: Student) => { 
    average = average + element.score
    
    if(element.score > HighestScore) {
      studentHighestScore = element.name
    }
  })

  average = average / array.length

  array.forEach((element: Student) => { 
       
    if(element.score >= average) {
      percentage++
    }
  })

  percentage = percentage / array.length * 100
  
  const result = {
    "Average" : average,
    "Highest score" : studentHighestScore,
    "Percentage" : percentage
  }

  
  res.send(result)
})

server.listen(3000)
