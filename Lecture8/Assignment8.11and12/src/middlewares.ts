import express, { Request, Response, NextFunction } from 'express'

interface ObjectWithRequiredFields {
  id: number,
  name: string,
  author: string,
  read: string
}

export const loggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const timestamp = new Date().toISOString()
  const method = req.method
  const url = req.originalUrl

  console.log(`[${timestamp}] ${method} ${url}`)

  next()
}

  export const validatorPost = ( req: Request, res: Response, next: NextFunction) => {
   const body = req.body

   const requiredKeys: (keyof ObjectWithRequiredFields)[] = ['id', 'name', 'author', 'read'];

   for (const key of requiredKeys) {
    if (!(key in body)) {
      res.status(400).send({ error: `Missing required field: ${key}` })
      return
    }
  }
    next()
}

export const validatorPut = ( req: Request, res: Response, next: NextFunction) => {
  const {name, author, read} = req.body

   if (!name && !author && !read) {
     res.status(400).send({ error: 'Missing required field' });
     return
   }
 
   next()
}

export const error404 = (req: Request, res: Response, next: NextFunction) => {
  res.status(404).send({error: 'no book for that ID'})

}
