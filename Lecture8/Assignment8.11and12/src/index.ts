import express, { Request, Response, NextFunction } from 'express'
import { validatorPost, error404, validatorPut, loggerMiddleware } from './middlewares'
import helmet from "helmet"

const server = express()
server.use(express.json())
server.use(loggerMiddleware)
server.use(helmet())

interface Book {
  id: number,
  name: string,
  author: string,
  read: string
}

let books: Book[] = []

server.get('/api/v1/books', (req: Request, res: Response) => {
  res.status(200).send(books)
})

server.get('/api/v1/books/:id', (req: Request, res: Response, next: NextFunction) => {
  const id = parseInt(req.params.id)
  const book = books.find(book => book.id === id)
  if(book) {
    res.status(200).send(book)
    return
  }
  next()
})

server.post('/api/v1/books', validatorPost, (req: Request, res: Response) => {
  validatorPost
  books.push(req.body)
  res.status(200).send()
})

server.put('/api/v1/books/:id', validatorPut, (req: Request, res: Response, next: NextFunction) => {
  const id = parseInt(req.params.id)
  const {name, author, read} = req.body
  const book = books.find(book => book.id === id)
  
  if(!book) {
    next()
    return
  }
  else{
    server.use(validatorPut)
  }
  if(name) {
    book.name = name
  }
  if(author) {
    book.author = author
  }
  if(read) {
    book.read = read
  }
  res.status(200).send(book)
  
})

server.delete('/api/v1/books/:id', (req: Request, res: Response, next: NextFunction) =>{
  const id = parseInt(req.params.id)
  const book = books.find(book => book.id === id)
  if(!book) {
    next()
    return
  } else {
    books = books.filter(book => book.id !== id)
  }
  res.status(200).send()
})

server.use(error404)

server.listen(3000)
