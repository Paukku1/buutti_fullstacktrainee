

const Info = ( props:any ) => {
    const title = <h1>Assignment 15.9 - Tic Tac Toe</h1>
    const firstParam = <p>Next player is Player {props.turn}</p>
    let status 
    if (props.winner) {
        status = props.winner === "Draw" ? "It's a draw!" : `Player ${props.winner} wins!`
    }
    
    return(
        <>
        <center>{title}</center>
        <center> {status ? status : firstParam}</center>
        </>
    )
}

export default Info