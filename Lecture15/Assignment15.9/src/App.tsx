import { useState } from 'react'
import Board from './Board'
import Info from './Info'


function App() {
  const [turn, setTurn] = useState('X')
  const [winner, setWinner] = useState<string | null>(null)

  const whoIsNext = (turn: string) => {
    if(turn === 'X'){
      setTurn('O')
    } else {
      setTurn('X')
    }
  }

  const handleWinnerChange = (newWinner: string | null) => {
    setWinner(newWinner);
  }

  return (
    <>
      <Info turn={turn} winner={winner}/>
      <Board turn={turn} whoIsNext={whoIsNext} winner={winner} onWinnerChange={handleWinnerChange}/>
    </>
  )
}

export default App
