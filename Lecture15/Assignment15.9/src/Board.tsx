import { useState, useRef} from "react";
import './Board.css'

interface Props {
    turn: string
    winner: string | null
    whoIsNext: (newTurn: string ) => void
    onWinnerChange: (newWinner: string | null) => void
}

const Board: React.FC<Props>  = ({turn, whoIsNext, winner, onWinnerChange}) => {
    const [board, setBoard] = useState(['', '', '', '', '', '', '', '', ''])
    const boardRef = useRef(null);

    const calculateWinner = (currentBoard: Array<string | null>): string | null => {
        const winningLines = [
          [1, 2, 3],
          [4, 5, 6],
          [7, 8, 9],
          [1, 4, 7],
          [2, 5, 8],
          [3, 6, 9],
          [1, 5, 9],
          [3, 5, 7]
        ]
    
        for (let i = 0; i < winningLines.length; i++) {
          const [a, b, c] = winningLines[i];
          
            if (
            currentBoard[a] &&
            currentBoard[a] === currentBoard[b] &&
            currentBoard[a] === currentBoard[c]
            ) {
            
            return currentBoard[a];
            }
        }

        let checkDraw = 0 
        
        currentBoard.forEach((square) => {
            if(square !== '') {
                checkDraw = checkDraw + 1
            }
        })

        if (checkDraw === 9) {
            return "Draw";
        }
    
        return null;
    }
    
    const handleClick = (index: number) => {
        if (board[index]  || winner) return;
    
        const newBoard = [...board]
        newBoard[index] = turn
        setBoard(newBoard)
        whoIsNext(turn)
        onWinnerChange(calculateWinner(newBoard))         
    }
    
    const drawSquare = (index: number) => {
        return <div className="input-inside" onClick={() => handleClick(index)}> {board[index]}</div>
        
    }

    const resetGame = () => {
        setBoard(Array(9).fill(''));
        whoIsNext('O')
        onWinnerChange(null)
    }
    
    return (
        <>        
        <div ref={boardRef} className="board">
            <div className="input input-1"> { drawSquare(1) } </div>
            <div className="input input-2"> { drawSquare(2) } </div>
            <div className="input input-3"> { drawSquare(3) } </div>
            <div className="input input-4"> { drawSquare(4) } </div>
            <div className="input input-5"> { drawSquare(5) } </div>
            <div className="input input-6"> { drawSquare(6) } </div>
            <div className="input input-7"> { drawSquare(7) } </div>
            <div className="input input-8"> { drawSquare(8) } </div>
            <div className="input input-9"> { drawSquare(9) } </div>
        
            <button className="reset-button" onClick={resetGame}>
                Reset Game
            </button>
        </div>    
    </>
    )
}

export default Board