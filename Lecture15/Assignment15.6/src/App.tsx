import NameList from "./NameList"


function App() {
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]

  return (
    <NameList names={namelist}/>
  )
}

export default App
