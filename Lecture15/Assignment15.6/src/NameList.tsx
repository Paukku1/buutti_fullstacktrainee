interface Props {
    names: Array<string>
}

function NameList(props: Props) {
    return (
      <ul>
        <h1>Assignment15.6</h1>
        {props.names.map((name, i) => {
  
          const content = i % 2 === 0
          ? <b>{name}</b>
          : <i>{name}</i>
          return <li key={'names' + i}><i>{content}</i></li>
          
        })}
        
      </ul>
    )
  }

  export default NameList