import { useState, ChangeEvent } from 'react'

const UserInput = () => {

    const [title, setTitle] = useState('')
    const [submitTitle, setSubmitTitle] = useState('Assignment15.7')
  
    const onTitleChange = (event: ChangeEvent<HTMLInputElement>) => {
        setTitle(event.target.value)
    }

    
    return(
        <div>
            <h1>{submitTitle}</h1>
            <input type='text'
            value={title}
            onChange={onTitleChange} />
    
            <button onClick={() => {setSubmitTitle(title)}}>
                Submit
            </button>
      </div>
    )
}

export default UserInput