import { useState, ChangeEvent, MouseEvent } from 'react'


function Todolist() {

  interface Props {
    id: number,
    text: string,
    done: boolean
  }
  const [newTodo, setNewToDo] = useState('')
  const [toDo, setToDo] = useState<Props[]>([])
  
    const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setNewToDo(event.target.value)
    }

    const addNewObject = (event: MouseEvent<HTMLButtonElement>) => {
      const newId = toDo.length > 0 ? toDo[toDo.length - 1].id + 1 : 1;
      
      const newT: Props = {
        id: newId,
        text: newTodo,
        done: false
      }
      
      setToDo([...toDo, newT])
    }

    const toggleTodo = (id: number) => {
      setToDo((prevTodos) =>
        prevTodos.map((todo) =>
          todo.id === id ? { ...todo, done: !todo.done } : todo
        )
      );
    };
  
    const removeTodo = (id: number) => {
      setToDo((prevTodos) => prevTodos.filter((todo) => todo.id !== id));
    }
  
  return (
    <>
      <input type='text'
            value={newTodo}
            onChange={onInputChange} />
    
      <button onClick={addNewObject}>
          Submit
      </button>
      <br />

      <ul>
        {toDo.map((todo) => (
          <li key={todo.id}>
            <input
              type="checkbox"
              checked={todo.done}
              onChange={() => toggleTodo(todo.id)}
            />
            <span style={{ textDecoration: todo.done ? "line-through" : "none" }}>
              {todo.text}
            </span>
            <button onClick={() => removeTodo(todo.id)}>Remove</button>
          </li>
        ))}
      </ul>
    </>
  )
}

export default Todolist
