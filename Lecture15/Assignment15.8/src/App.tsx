import Todolist from "./Todolist"

function App() {
  const title = <h1>Assignment 15.8</h1>
  return (
    <>
      {title}
      <Todolist />
    </>
  )
}

export default App
