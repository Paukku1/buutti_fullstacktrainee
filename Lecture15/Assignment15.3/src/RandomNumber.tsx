const RandomNumber = () => {
    const header = <h1>Random Number Generator</h1>
    const randomNumber = `Your Random Number is: ${Math.floor(Math.random() * 100)}!`
    return (
        <div>
            { header }
           <p> { randomNumber } </p>
            
        </div>
    )
}

export default RandomNumber