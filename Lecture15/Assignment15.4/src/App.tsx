import Instrument from "./Instrument"

function App() {

  return (
    <>
    <Instrument name='Guitar' image='../images/guitar.png' price={190} />
    <Instrument name='Violin' image='../images/violin.png' price={280} />
    <Instrument name='Tuba' image='../images/tuba.png' price={1190} />
    </>
  )
}

export default App
