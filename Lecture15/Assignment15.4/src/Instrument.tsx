import './Instrumet.css'
interface Props {
    name: string,
    image: string,
    price: number
}

const Instrument = (props: Props) => {
    return (
        <>  
            <div className='imgDiv'><img src={props.image} height='100px' /> 
            <div className='txtDiv'>
                <p>{props.name}</p>
                <p>{props.price} e</p>
            </div>
            </div>
        </>
    )
}

export default Instrument